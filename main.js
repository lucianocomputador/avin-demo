(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/luciano/workspace/avin-ui/src/main.ts */"zUnb");


/***/ }),

/***/ "19mU":
/*!*************************************!*\
  !*** ./src/app/menu/menu.module.ts ***!
  \*************************************/
/*! exports provided: MenuModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuModule", function() { return MenuModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _menu_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./menu.component */ "TGkX");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "R1ws");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/toolbar */ "5EWq");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_splitbutton__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/splitbutton */ "Wq6t");
/* harmony import */ var primeng_tabview__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/tabview */ "dPl2");
/* harmony import */ var primeng_codehighlighter__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/codehighlighter */ "yjSK");
/* harmony import */ var primeng_sidebar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/sidebar */ "jLSX");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var _dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../dashboard/dashboard.module */ "TDBs");
/* harmony import */ var _breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../breadcrumb/breadcrumb.component */ "55oq");
/* harmony import */ var _painel_apropriador_painel_apropriador_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../painel-apropriador/painel-apropriador.module */ "ud/B");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/core */ "fXoL");













// import {BreadcrumbModule} from 'primeng/breadcrumb';
// import {MenuItem} from 'primeng/api';


class MenuModule {
}
MenuModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_14__["ɵɵdefineNgModule"]({ type: MenuModule });
MenuModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_14__["ɵɵdefineInjector"]({ factory: function MenuModule_Factory(t) { return new (t || MenuModule)(); }, providers: [], imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
            primeng_toolbar__WEBPACK_IMPORTED_MODULE_4__["ToolbarModule"],
            primeng_button__WEBPACK_IMPORTED_MODULE_5__["ButtonModule"],
            primeng_splitbutton__WEBPACK_IMPORTED_MODULE_6__["SplitButtonModule"],
            primeng_tabview__WEBPACK_IMPORTED_MODULE_7__["TabViewModule"],
            primeng_codehighlighter__WEBPACK_IMPORTED_MODULE_8__["CodeHighlighterModule"],
            primeng_sidebar__WEBPACK_IMPORTED_MODULE_9__["SidebarModule"],
            primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__["BreadcrumbModule"],
            _dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_11__["DashboardModule"],
            _painel_apropriador_painel_apropriador_module__WEBPACK_IMPORTED_MODULE_13__["PainelApropriadorModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_14__["ɵɵsetNgModuleScope"](MenuModule, { declarations: [_menu_component__WEBPACK_IMPORTED_MODULE_1__["MenuComponent"], _breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_12__["BreadcrumbComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
        primeng_toolbar__WEBPACK_IMPORTED_MODULE_4__["ToolbarModule"],
        primeng_button__WEBPACK_IMPORTED_MODULE_5__["ButtonModule"],
        primeng_splitbutton__WEBPACK_IMPORTED_MODULE_6__["SplitButtonModule"],
        primeng_tabview__WEBPACK_IMPORTED_MODULE_7__["TabViewModule"],
        primeng_codehighlighter__WEBPACK_IMPORTED_MODULE_8__["CodeHighlighterModule"],
        primeng_sidebar__WEBPACK_IMPORTED_MODULE_9__["SidebarModule"],
        primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_10__["BreadcrumbModule"],
        _dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_11__["DashboardModule"],
        _painel_apropriador_painel_apropriador_module__WEBPACK_IMPORTED_MODULE_13__["PainelApropriadorModule"]], exports: [_menu_component__WEBPACK_IMPORTED_MODULE_1__["MenuComponent"]] }); })();


/***/ }),

/***/ "1ZoJ":
/*!*****************************************************************!*\
  !*** ./src/app/acoes-de-melhorias/acoes-de-melhorias.module.ts ***!
  \*****************************************************************/
/*! exports provided: AcoesDeMelhoriasModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcoesDeMelhoriasModule", function() { return AcoesDeMelhoriasModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _acoes_de_melhorias_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./acoes-de-melhorias-routing.module */ "byP8");
/* harmony import */ var _acoes_de_melhorias_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./acoes-de-melhorias.component */ "p9TD");
/* harmony import */ var _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/primeng.module */ "Cp9f");
/* harmony import */ var _form_acao_melhoria_form_acao_melhoria_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./form-acao-melhoria/form-acao-melhoria.component */ "VLMe");
/* harmony import */ var _impedimentos_impedimentos_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../impedimentos/impedimentos.module */ "tRTB");
/* harmony import */ var _comprovacoes_comprovacoes_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../comprovacoes/comprovacoes.module */ "vKON");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ "fXoL");








class AcoesDeMelhoriasModule {
}
AcoesDeMelhoriasModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineNgModule"]({ type: AcoesDeMelhoriasModule });
AcoesDeMelhoriasModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineInjector"]({ factory: function AcoesDeMelhoriasModule_Factory(t) { return new (t || AcoesDeMelhoriasModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__["PrimengModule"],
            _acoes_de_melhorias_routing_module__WEBPACK_IMPORTED_MODULE_1__["AcoesDeMelhoriasRoutingModule"],
            _impedimentos_impedimentos_module__WEBPACK_IMPORTED_MODULE_5__["ImpedimentosModule"],
            _comprovacoes_comprovacoes_module__WEBPACK_IMPORTED_MODULE_6__["ComprovacoesModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵsetNgModuleScope"](AcoesDeMelhoriasModule, { declarations: [_acoes_de_melhorias_component__WEBPACK_IMPORTED_MODULE_2__["AcoesDeMelhoriasComponent"], _form_acao_melhoria_form_acao_melhoria_component__WEBPACK_IMPORTED_MODULE_4__["FormAcaoMelhoriaComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__["PrimengModule"],
        _acoes_de_melhorias_routing_module__WEBPACK_IMPORTED_MODULE_1__["AcoesDeMelhoriasRoutingModule"],
        _impedimentos_impedimentos_module__WEBPACK_IMPORTED_MODULE_5__["ImpedimentosModule"],
        _comprovacoes_comprovacoes_module__WEBPACK_IMPORTED_MODULE_6__["ComprovacoesModule"]], exports: [_acoes_de_melhorias_component__WEBPACK_IMPORTED_MODULE_2__["AcoesDeMelhoriasComponent"]] }); })();


/***/ }),

/***/ "55oq":
/*!****************************************************!*\
  !*** ./src/app/breadcrumb/breadcrumb.component.ts ***!
  \****************************************************/
/*! exports provided: BreadcrumbComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BreadcrumbComponent", function() { return BreadcrumbComponent; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
// import { isNullOrUndefined, log } from 'util';





class BreadcrumbComponent {
    constructor(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.home = { icon: 'pi pi-home', url: '/' };
        this.menuItems0 = [];
        this.url = [
            {
                "label": "Painel Apropriador"
            },
            {
                "label": "Tópicos",
                "url": "/topicos"
            }
        ];
        this.urlRaiz = [
            '#/dashboard',
            '#/painelapropriador',
        ];
    }
    ngOnInit() {
        this.router.events
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["filter"])(event => event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_0__["NavigationEnd"]))
            .subscribe(() => this.menuItems = this.createBreadcrumbs(this.activatedRoute.root));
    }
    createBreadcrumbs(route, url = '', breadcrumbs = [], label = '') {
        const children = route.children;
        // pegar url com atributos
        const state = this.router.routerState;
        const snapshot = state.snapshot;
        if (children.length === 0) {
            return breadcrumbs;
        }
        for (const child of children) {
            // pega url sem atributos
            // const routeURL: string = child.snapshot.url.map(segment => segment.path).join('/');
            // pegar url com atributos
            const routeURL = snapshot.url;
            if (routeURL !== '') {
                url += `#${routeURL}`;
            }
            // Se url solicitada pertence ao menu so mostrar ela ou seja zera breadcrumb
            if (this.verificaUrlRaiz(url)) {
                this.menuItems0 = [];
            }
            label += child.snapshot.data[BreadcrumbComponent.ROUTE_DATA_BREADCRUMB];
            // Luciano simplificando
            let bread = { label: label, url: url };
            let breadSemURL = { label: label };
            this.menuItems0.push(bread);
            if (label !== 'undefined') {
                // Adicionar primeira url se breadcrumb vazio
                if (this.menuItems0.length == 0) {
                    this.menuItems0.push(bread);
                }
                else {
                    // verificar label existe em breadcrumb e limpar os demais
                    if (!this.isExit(this.menuItems0, label)) {
                        this.menuItems0.push(bread);
                    }
                    else {
                        let menuItemsTmp = [];
                        var keepGoing = true;
                        this.menuItems0.forEach(value => {
                            if (keepGoing) {
                                menuItemsTmp.push(value);
                                if (value.label == label) {
                                    this.menuItems0 = menuItemsTmp;
                                    keepGoing = false;
                                }
                            }
                        });
                    }
                }
            }
        }
    }
    isExit(itens, valor) {
        let resposta;
        itens.forEach(value => {
            if (value.label == valor) {
                resposta = true;
            }
            else if (resposta != true) {
                resposta = false;
            }
        });
        return resposta;
    }
    verificaUrlRaiz(url) {
        let resposta = false;
        this.urlRaiz.forEach(valor => {
            if (valor === url)
                resposta = true;
        });
        return resposta;
    }
}
BreadcrumbComponent.ROUTE_DATA_BREADCRUMB = 'breadcrumb';
BreadcrumbComponent.ɵfac = function BreadcrumbComponent_Factory(t) { return new (t || BreadcrumbComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_0__["ActivatedRoute"])); };
BreadcrumbComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: BreadcrumbComponent, selectors: [["app-breadcrumb"]], decls: 1, vars: 2, consts: [["id", "xmenu", "styleClass", "my-breadcrumb", 3, "home", "model"]], template: function BreadcrumbComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "p-breadcrumb", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("home", ctx.home)("model", ctx.menuItems0);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_3__["Breadcrumb"]], styles: ["[_nghost-%COMP%]     .my-breadcrumb {\n  padding-top: 5px;\n  padding-bottom: 20px;\n  height: 24px;\n}\n[_nghost-%COMP%]     .my-breadcrumb .p-menuitem-icon {\n  color: #00a650 !important;\n}\n[_nghost-%COMP%]     .my-breadcrumb .p-menuitem-text {\n  color: #00a650 !important;\n}\n[_nghost-%COMP%]     .my-breadcrumb .p-breadcrumb-chevron {\n  color: #00a650 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL2JyZWFkY3J1bWIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUk7RUFDSSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsWUFBQTtBQURSO0FBTUk7RUFDSSx5QkFBQTtBQUpSO0FBUUk7RUFDSSx5QkFBQTtBQU5SO0FBVUk7RUFDSSx5QkFBQTtBQVJSIiwiZmlsZSI6ImJyZWFkY3J1bWIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBSZWVzY3JldmVuZG8gdGhlbWFcbjpob3N0IHtcbiAgICA6Om5nLWRlZXAgLm15LWJyZWFkY3J1bWIge1xuICAgICAgICBwYWRkaW5nLXRvcDogNXB4O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbiAgICAgICAgaGVpZ2h0OiAyNHB4O1xuICAgICBcbiAgICB9XG5cbiAgICAvLyBob21lXG4gICAgOjpuZy1kZWVwIC5teS1icmVhZGNydW1iIC5wLW1lbnVpdGVtLWljb24ge1xuICAgICAgICBjb2xvcjogIzAwYTY1MCAhaW1wb3J0YW50O1xuICAgICAgICAvLyBmb250LXNpemU6IDEwcHg7XG4gICAgfVxuICAgIC8vIHRleHRvXG4gICAgOjpuZy1kZWVwIC5teS1icmVhZGNydW1iIC5wLW1lbnVpdGVtLXRleHQge1xuICAgICAgICBjb2xvcjogIzAwYTY1MCAhaW1wb3J0YW50O1xuICAgICAgICAvLyBmb250LXNpemU6IDEwcHg7XG4gICAgfVxuICAgIC8vIHNlcGFyYWRvciA+XG4gICAgOjpuZy1kZWVwIC5teS1icmVhZGNydW1iIC5wLWJyZWFkY3J1bWItY2hldnJvbiB7XG4gICAgICAgIGNvbG9yOiAjMDBhNjUwICFpbXBvcnRhbnQ7XG4gICAgICAgIC8vIGZvbnQtc2l6ZTogMTBweDtcbiAgICB9XG5cbn1cbiJdfQ== */"] });


/***/ }),

/***/ "5pU/":
/*!********************************************************************!*\
  !*** ./src/app/painel-apropriador/painel-apropriador.component.ts ***!
  \********************************************************************/
/*! exports provided: PainelApropriadorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PainelApropriadorComponent", function() { return PainelApropriadorComponent; });
/* harmony import */ var _core_GAS__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../core/GAS */ "Y7Y+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_dataview__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/dataview */ "8CEF");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/dropdown */ "arFO");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var primeng_multiselect__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/multiselect */ "lVkt");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/button */ "jIHw");













function PainelApropriadorComponent_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "label", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "CICLO");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "p-dropdown", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function PainelApropriadorComponent_ng_template_2_Template_p_dropdown_ngModelChange_3_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r3.selectedCiclo = $event; })("onChange", function PainelApropriadorComponent_ng_template_2_Template_p_dropdown_onChange_3_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r5.filtra($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "span", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "label", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "SEGMENTOS");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "p-multiSelect", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function PainelApropriadorComponent_ng_template_2_Template_p_multiSelect_ngModelChange_7_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r6.selectedSegmentos = $event; })("onChange", function PainelApropriadorComponent_ng_template_2_Template_p_multiSelect_onChange_7_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r7.filtra($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "span", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "label", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10, "CAMPUS");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "p-dropdown", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function PainelApropriadorComponent_ng_template_2_Template_p_dropdown_ngModelChange_11_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r8.selectedCampus = $event; })("onChange", function PainelApropriadorComponent_ng_template_2_Template_p_dropdown_onChange_11_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r9.filtra($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "span", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](13, "i", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "input", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("keyup", function PainelApropriadorComponent_ng_template_2_Template_input_keyup_14_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4); _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](1); return _r0.filter($event.target.value); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.ciclos)("ngModel", ctx_r1.selectedCiclo);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.segmentos)("ngModel", ctx_r1.selectedSegmentos);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.campi)("ngModel", ctx_r1.selectedCampus);
} }
function PainelApropriadorComponent_ng_template_3_ng_template_1_span_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" \u2630", item_r11.nivelOrganizacional.subnivel.nome, " ");
} }
function PainelApropriadorComponent_ng_template_3_ng_template_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "table", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "td", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, PainelApropriadorComponent_ng_template_3_ng_template_1_span_5_Template, 2, 1, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "td", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "span", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, " PAINEL ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "span", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "span", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" \u2630", item_r11.nivelOrganizacional.nome, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r11.nivelOrganizacional.subnivel != undefined);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" \u2630", item_r11.segmento, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" \u2630", item_r11.ciclo, " ");
} }
function PainelApropriadorComponent_ng_template_3_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    const _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "table", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "td", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "img", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "span", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, " | ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "span", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, " t\u00F3picos ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](10, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, " | ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "span", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14, " dimens\u00F5es ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](16, "img", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "span", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18, " | ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "span", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, " t\u00F3picos | ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "span", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](24, " com a\u00E7\u00F5es ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](25, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](26, " | ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "span", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](28);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](29, " dimens\u00F5es ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "td", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](31, "p-button", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function PainelApropriadorComponent_ng_template_3_ng_template_2_Template_p_button_click_31_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r19); const item_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r17.click(item_r11.id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r11.potencialidade.topicos);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r11.potencialidade.dimensoes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r11.fragilidades.topicos);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r11.fragilidades.acoes);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r11.fragilidades.dimensoes);
} }
function PainelApropriadorComponent_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-card", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, PainelApropriadorComponent_ng_template_3_ng_template_1_Template, 17, 4, "ng-template", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, PainelApropriadorComponent_ng_template_3_ng_template_2_Template, 32, 5, "ng-template", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("id", ctx_r2.id);
} }
class PainelApropriadorComponent {
    constructor(router, gas) {
        this.router = router;
        this.gas = gas;
        this.ciclos = [];
        this.campi = [];
        this.segmentos = [];
        this.lista = [];
        this.listaAux = [];
    }
    ngOnInit() {
        this.getLista();
    }
    getLista() {
        _core_GAS__WEBPACK_IMPORTED_MODULE_0__["REQUESTGAS"].functionName = _core_GAS__WEBPACK_IMPORTED_MODULE_0__["RESOURCE_PAINEL_APROPRIADOR"].functionName;
        _core_GAS__WEBPACK_IMPORTED_MODULE_0__["REQUESTGAS"].functionParams = [];
        this.gas.GASRequest(_core_GAS__WEBPACK_IMPORTED_MODULE_0__["REQUESTGAS"], this.resultadoPainelApropriador, this);
        // this.resultadoPainelApropriador('fake');
    }
    resultadoPainelApropriador(response) {
        console.log('Resposta');
        console.log(response);
        this.tratarResposta(response.response);
    }
    fake() {
        return [
            {
                id: 1,
                segmento: 'DOCENTE',
                nivelOrganizacional: {
                    tipo: 'CAMPUS',
                    nome: 'CAMPUS-JP',
                    emailResponsaveis: ['@', '@'],
                    subnivel: {
                        tipo: 'CURSO',
                        nome: '502 - Tecnologia em Análise e Desenvolvimento de Sistemas - Monteiro (CAMPUS MONTEIRO)',
                        emailResponsaveis: ['@', '@'],
                    }
                },
                dataInicio: '05/01/2021',
                dataTermino: '01/01/2023',
                ciclo: '2021-2023',
                objetivo: 'Verificar qualidade do ensino do curso.',
                itemEscalaAvaliativa: [{ valorQuantitativo: 'ItemEscalaAvaliativa', valorQualitativo: '' }],
                potencialidade: {
                    topicos: 999,
                    dimensoes: 9,
                },
                fragilidades: {
                    topicos: 999,
                    dimensoes: 9,
                    acoes: 99,
                },
            },
            {
                id: 1,
                segmento: 'DISCENTE',
                nivelOrganizacional: {
                    tipo: 'CAMPUS',
                    nome: 'CAMPUS-MT',
                    emailResponsaveis: ['@', '@'],
                    subnivel: {
                        tipo: 'CURSO',
                        nome: '501 - Tecnologia em Análise e Desenvolvimento de Sistemas - Monteiro (CAMPUS MONTEIRO)',
                        emailResponsaveis: ['@', '@'],
                    }
                },
                dataInicio: '10/01/2021',
                dataTermino: '01/01/2023',
                ciclo: '2022-2023',
                objetivo: 'Atender qualidade do ensino do curso.',
                itemEscalaAvaliativa: [{ valorQuantitativo: 'ItemEscalaAvaliativa', valorQualitativo: '' }],
                potencialidade: {
                    topicos: 999,
                    dimensoes: 9,
                },
                fragilidades: {
                    topicos: 999,
                    dimensoes: 9,
                    acoes: 99,
                },
            }
        ];
    }
    tratarResposta(response) {
        let listaPaineis = response;
        // listapaineis fake    
        // let listaPaineis = this.fake();
        this.listaAux = listaPaineis; // Para auxiliar nos filtros
        this.lista = listaPaineis;
        // Atualiza campos de filtro conforme informações existente na lista
        this.getCiclos(this.lista);
        this.getCampus(this.lista);
        this.getSegmentos(this.lista);
    }
    /**
     * Prepara itens para seleção do filtro os ciclos exixtente na lista recebida
     * @param lista
     */
    getCiclos(lista) {
        this.ciclos = [];
        this.ciclos.push({ ciclo: 'TODOS' });
        lista.forEach(element => {
            this.ciclos.push({ ciclo: element.ciclo });
        });
    }
    /**
     * Prepara itens para seleção do filtro os campus exixtente na lista recebida
     * @param lista
     */
    getCampus(lista) {
        this.campi = [];
        this.campi.push({ campus: 'TODOS' });
        lista.forEach(element => {
            this.campi.push({ campus: element.nivelOrganizacional.nome });
        });
    }
    /**
     * Prepara itens para seleção do filtro os segmentos exixtente na lista recebida
     * @param lista
     */
    getSegmentos(lista) {
        this.segmentos = [];
        lista.forEach(element => {
            this.segmentos.push({ segmento: element.segmento });
        });
    }
    /**
     * Abre link
     * @param url
     */
    click(id) {
        this.router.navigate(['/topicos'], { queryParams: { 'painel': id }, skipLocationChange: false });
    }
    /**
     * Filta pelos campos de filtro utiliza uma lista auxiliar para não perder o getLista inicial
     * E atualiza os outro campos de filtro apenas para conter informaçoes do que foi filtrado
     * @param event
     */
    filtra(event) {
        this.lista = this.listaAux;
        this.getCiclos(this.lista);
        this.getCampus(this.lista);
        this.getSegmentos(this.lista);
        if (this.selectedCiclo != undefined && this.selectedCiclo.ciclo != 'TODOS') {
            let ciclo = this.selectedCiclo.ciclo;
            this.lista = this.listaAux.filter(function (key) {
                return key.ciclo == ciclo;
            });
            this.getCampus(this.lista);
            this.getSegmentos(this.lista);
        }
        if (this.selectedSegmentos != undefined && this.selectedSegmentos.length > 0) {
            let segmentosFilter = this.selectedSegmentos.map(item => { return item.segmento; });
            this.lista = this.lista.filter(key => segmentosFilter.includes(key.segmento));
            this.getCiclos(this.lista);
            this.getCampus(this.lista);
        }
        if (this.selectedCampus != undefined) {
            let campus = this.selectedCampus.campus;
            let campusFilter = this.campi.map(item => { return item.campus; });
            if (campus == 'TODOS') {
                this.lista = this.lista.filter(key => campusFilter.includes(key.nivelOrganizacional.nome));
            }
            else {
                this.lista = this.lista.filter(function (key) {
                    return key.nivelOrganizacional.nome === campus;
                });
            }
            this.getCiclos(this.lista);
            this.getSegmentos(this.lista);
        }
    }
}
PainelApropriadorComponent.ɵfac = function PainelApropriadorComponent_Factory(t) { return new (t || PainelApropriadorComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_core_GAS__WEBPACK_IMPORTED_MODULE_0__["GAS"])); };
PainelApropriadorComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: PainelApropriadorComponent, selectors: [["app-painel-apropriador"]], decls: 4, vars: 3, consts: [["paginatorPosition", "both", "filterBy", "ciclo,dataInicio,nivelOrganizacional.nome,nivelOrganizacional.subnivel.nome", "layout", "grid", 3, "value", "paginator", "rows"], ["dv", ""], ["pTemplate", "header"], ["pTemplate", "gridItem"], [1, "block"], ["for", "dropdownCiclo", 1, "label"], ["id", "dropdownCiclo", "optionLabel", "ciclo", 3, "options", "ngModel", "ngModelChange", "onChange"], ["for", "dropdownSegmento", 1, "label"], ["id", "dropdownSegmento", "defaultLabel", "TODOS", "optionLabel", "segmento", "display", "chip", 3, "options", "ngModel", "ngModelChange", "onChange"], ["for", "dropdownCampus", 1, "label"], ["id", "dropdownCampus", "optionLabel", "campus", 3, "options", "ngModel", "ngModelChange", "onChange"], [1, "p-input-icon-left"], [1, "pi", "pi-search"], ["type", "search", "pInputText", "", "placeholder", "Pesquisar", 3, "keyup"], [3, "id"], ["pTemplate", "footer"], [1, "tabela"], [1, "col80"], [1, "tagTitulo"], ["class", "tagTitulo", 4, "ngIf"], ["rowspan", "3"], [1, "tituloCard"], [1, "tagSubtitulo"], [1, "divSentimento"], ["src", "../../assets/icons/faceAzul.svg"], [1, "contextosPotencialidade"], [1, "contextosNumero"], ["src", "../../assets/icons/faceVermelha.svg"], [1, "contextosFragilidade"], [1, "botoesCard"], ["icon", "bi bi-megaphone", "iconPos", "left", "title", "T\u00F3picos", "styleClass", "iconBotao", 3, "click"]], template: function PainelApropriadorComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-dataView", 0, 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, PainelApropriadorComponent_ng_template_2_Template, 15, 6, "ng-template", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, PainelApropriadorComponent_ng_template_3_Template, 3, 1, "ng-template", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("value", ctx.lista)("paginator", true)("rows", 9);
    } }, directives: [primeng_dataview__WEBPACK_IMPORTED_MODULE_3__["DataView"], primeng_api__WEBPACK_IMPORTED_MODULE_4__["PrimeTemplate"], primeng_dropdown__WEBPACK_IMPORTED_MODULE_5__["Dropdown"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgModel"], primeng_multiselect__WEBPACK_IMPORTED_MODULE_7__["MultiSelect"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_8__["InputText"], primeng_card__WEBPACK_IMPORTED_MODULE_9__["Card"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["NgIf"], primeng_button__WEBPACK_IMPORTED_MODULE_11__["Button"]], styles: ["[_nghost-%COMP%]     .iconBotao {\n  font-size: 1.5rem !important;\n  padding-bottom: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3BhaW5lbC1hcHJvcHJpYWRvci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLDRCQUFBO0VBQ0EsbUJBQUE7QUFBUiIsImZpbGUiOiJwYWluZWwtYXByb3ByaWFkb3IuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgOjpuZy1kZWVwIC5pY29uQm90YW8ge1xuICAgICAgICBmb250LXNpemU6IDEuNXJlbSAhaW1wb3J0YW50O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICAgIH0gICAgXG59XG4iXX0= */"] });


/***/ }),

/***/ "8FQr":
/*!*************************************************************!*\
  !*** ./src/app/comprovacoes/comprovacoes-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: ComprovacoesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComprovacoesRoutingModule", function() { return ComprovacoesRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _comprovacoes_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./comprovacoes.component */ "gFHE");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");




const routes = [
    { path: 'comprovacoes',
        component: _comprovacoes_component__WEBPACK_IMPORTED_MODULE_1__["ComprovacoesComponent"],
        data: {
            title: 'Comprovações',
            breadcrumb: 'Comprovações'
        }
    },
];
class ComprovacoesRoutingModule {
}
ComprovacoesRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: ComprovacoesRoutingModule });
ComprovacoesRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ factory: function ComprovacoesRoutingModule_Factory(t) { return new (t || ComprovacoesRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](ComprovacoesRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    breadcrumb: [],
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "Cp9f":
/*!****************************************!*\
  !*** ./src/app/core/primeng.module.ts ***!
  \****************************************/
/*! exports provided: PrimengModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrimengModule", function() { return PrimengModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dropdown */ "arFO");
/* harmony import */ var primeng_dataview__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/dataview */ "8CEF");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_multiselect__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/multiselect */ "lVkt");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_radiobutton__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/radiobutton */ "LuMj");
/* harmony import */ var primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/inputtextarea */ "zFJ7");
/* harmony import */ var primeng_panel__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/panel */ "7CaW");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/table */ "rEr+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/core */ "fXoL");














class PrimengModule {
}
PrimengModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_13__["ɵɵdefineNgModule"]({ type: PrimengModule });
PrimengModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_13__["ɵɵdefineInjector"]({ factory: function PrimengModule_Factory(t) { return new (t || PrimengModule)(); }, providers: [], imports: [[], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
        primeng_card__WEBPACK_IMPORTED_MODULE_5__["CardModule"],
        primeng_dropdown__WEBPACK_IMPORTED_MODULE_2__["DropdownModule"],
        primeng_dataview__WEBPACK_IMPORTED_MODULE_3__["DataViewModule"],
        primeng_inputtext__WEBPACK_IMPORTED_MODULE_4__["InputTextModule"],
        primeng_button__WEBPACK_IMPORTED_MODULE_6__["ButtonModule"],
        primeng_multiselect__WEBPACK_IMPORTED_MODULE_7__["MultiSelectModule"],
        primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_8__["DynamicDialogModule"],
        primeng_radiobutton__WEBPACK_IMPORTED_MODULE_9__["RadioButtonModule"],
        primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_10__["InputTextareaModule"],
        primeng_panel__WEBPACK_IMPORTED_MODULE_11__["PanelModule"],
        primeng_table__WEBPACK_IMPORTED_MODULE_12__["TableModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_13__["ɵɵsetNgModuleScope"](PrimengModule, { exports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
        primeng_card__WEBPACK_IMPORTED_MODULE_5__["CardModule"],
        primeng_dropdown__WEBPACK_IMPORTED_MODULE_2__["DropdownModule"],
        primeng_dataview__WEBPACK_IMPORTED_MODULE_3__["DataViewModule"],
        primeng_inputtext__WEBPACK_IMPORTED_MODULE_4__["InputTextModule"],
        primeng_button__WEBPACK_IMPORTED_MODULE_6__["ButtonModule"],
        primeng_multiselect__WEBPACK_IMPORTED_MODULE_7__["MultiSelectModule"],
        primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_8__["DynamicDialogModule"],
        primeng_radiobutton__WEBPACK_IMPORTED_MODULE_9__["RadioButtonModule"],
        primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_10__["InputTextareaModule"],
        primeng_panel__WEBPACK_IMPORTED_MODULE_11__["PanelModule"],
        primeng_table__WEBPACK_IMPORTED_MODULE_12__["TableModule"]] }); })();


/***/ }),

/***/ "Cqa6":
/*!*******************************************!*\
  !*** ./src/app/topicos/topicos.module.ts ***!
  \*******************************************/
/*! exports provided: TopicosModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopicosModule", function() { return TopicosModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _topicos_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./topicos-routing.module */ "H+m0");
/* harmony import */ var _topicos_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./topicos.component */ "R69Q");
/* harmony import */ var _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/primeng.module */ "Cp9f");
/* harmony import */ var _form_topico_form_topico_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./form-topico/form-topico.component */ "fzUc");
/* harmony import */ var _acoes_de_melhorias_acoes_de_melhorias_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../acoes-de-melhorias/acoes-de-melhorias.module */ "1ZoJ");
/* harmony import */ var _excluir_excluir_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../excluir/excluir.component */ "pJC4");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ "fXoL");








class TopicosModule {
}
TopicosModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineNgModule"]({ type: TopicosModule });
TopicosModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineInjector"]({ factory: function TopicosModule_Factory(t) { return new (t || TopicosModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__["PrimengModule"],
            _topicos_routing_module__WEBPACK_IMPORTED_MODULE_1__["TopicosRoutingModule"],
            _acoes_de_melhorias_acoes_de_melhorias_module__WEBPACK_IMPORTED_MODULE_5__["AcoesDeMelhoriasModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵsetNgModuleScope"](TopicosModule, { declarations: [_topicos_component__WEBPACK_IMPORTED_MODULE_2__["TopicosComponent"], _form_topico_form_topico_component__WEBPACK_IMPORTED_MODULE_4__["FormTopicoComponent"], _excluir_excluir_component__WEBPACK_IMPORTED_MODULE_6__["ExcluirComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__["PrimengModule"],
        _topicos_routing_module__WEBPACK_IMPORTED_MODULE_1__["TopicosRoutingModule"],
        _acoes_de_melhorias_acoes_de_melhorias_module__WEBPACK_IMPORTED_MODULE_5__["AcoesDeMelhoriasModule"]], exports: [_topicos_component__WEBPACK_IMPORTED_MODULE_2__["TopicosComponent"]] }); })();


/***/ }),

/***/ "Cxho":
/*!*****************************************************************************!*\
  !*** ./src/app/impedimentos/form-impedimento/form-impedimento.component.ts ***!
  \*****************************************************************************/
/*! exports provided: FormImpedimentoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormImpedimentoComponent", function() { return FormImpedimentoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/inputtextarea */ "zFJ7");





class FormImpedimentoComponent {
    constructor(ref, config) {
        this.ref = ref;
        this.config = config;
    }
    ngOnInit() {
    }
    close() {
        this.ref.close();
    }
    confirme() {
        this.ref.close('confirm');
    }
}
FormImpedimentoComponent.ɵfac = function FormImpedimentoComponent_Factory(t) { return new (t || FormImpedimentoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_1__["DynamicDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_1__["DynamicDialogConfig"])); };
FormImpedimentoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: FormImpedimentoComponent, selectors: [["app-form-impedimento"]], decls: 7, vars: 2, consts: [[1, "buttons"], ["pButton", "", "pRipple", "", "type", "button", "label", "SALVAR", 3, "click"], ["pButton", "", "pRipple", "", "type", "button", "label", "CANCELAR", 1, "espacoEsqueda", 3, "click"], [1, "espacoRow", "min"], ["for", "textoToipico", 1, "label"], ["id", "textoToipico", "autoResize", "false", "maxlength", "500", "pInputTextarea", "", 3, "rows", "ngModel", "ngModelChange"]], template: function FormImpedimentoComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FormImpedimentoComponent_Template_button_click_1_listener() { return ctx.confirme(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FormImpedimentoComponent_Template_button_click_2_listener() { return ctx.close(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "label", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "NOVO IMPEDIMENTO");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "textarea", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function FormImpedimentoComponent_Template_textarea_ngModelChange_6_listener($event) { return ctx.impedimento = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rows", 5)("ngModel", ctx.impedimento);
    } }, directives: [primeng_button__WEBPACK_IMPORTED_MODULE_2__["ButtonDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["MaxLengthValidator"], primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_4__["InputTextarea"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgModel"]], styles: [".espacoEsqueda[_ngcontent-%COMP%] {\n  margin-left: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2Zvcm0taW1wZWRpbWVudG8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtBQUNKIiwiZmlsZSI6ImZvcm0taW1wZWRpbWVudG8uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXNwYWNvRXNxdWVkYSB7XG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcbn0iXX0= */"] });


/***/ }),

/***/ "GHM7":
/*!*************************************************************!*\
  !*** ./src/app/impedimentos/impedimentos-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: ImpedimentosRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImpedimentosRoutingModule", function() { return ImpedimentosRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _impedimentos_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./impedimentos.component */ "NXVx");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");




const routes = [
    { path: 'impedimentos',
        component: _impedimentos_component__WEBPACK_IMPORTED_MODULE_1__["ImpedimentosComponent"],
        data: {
            title: 'Impedimentos',
            breadcrumb: 'Impedimentos'
        }
    },
];
class ImpedimentosRoutingModule {
}
ImpedimentosRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: ImpedimentosRoutingModule });
ImpedimentosRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ factory: function ImpedimentosRoutingModule_Factory(t) { return new (t || ImpedimentosRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](ImpedimentosRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "H+m0":
/*!***************************************************!*\
  !*** ./src/app/topicos/topicos-routing.module.ts ***!
  \***************************************************/
/*! exports provided: TopicosRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopicosRoutingModule", function() { return TopicosRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _topicos_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./topicos.component */ "R69Q");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");




const routes = [
    { path: 'topicos',
        component: _topicos_component__WEBPACK_IMPORTED_MODULE_1__["TopicosComponent"],
        data: {
            title: 'Tópicos',
            breadcrumb: 'Tópicos'
        }
    },
];
class TopicosRoutingModule {
}
TopicosRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: TopicosRoutingModule });
TopicosRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ factory: function TopicosRoutingModule_Factory(t) { return new (t || TopicosRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](TopicosRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "HUMg":
/*!*****************************************************************************!*\
  !*** ./src/app/comprovacoes/form-comprovacao/form-comprovacao.component.ts ***!
  \*****************************************************************************/
/*! exports provided: FormComprovacaoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormComprovacaoComponent", function() { return FormComprovacaoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/inputtextarea */ "zFJ7");





class FormComprovacaoComponent {
    constructor(ref, config) {
        this.ref = ref;
        this.config = config;
    }
    ngOnInit() {
    }
    close() {
        this.ref.close();
    }
    confirme() {
        this.ref.close('confirm');
    }
}
FormComprovacaoComponent.ɵfac = function FormComprovacaoComponent_Factory(t) { return new (t || FormComprovacaoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_1__["DynamicDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_1__["DynamicDialogConfig"])); };
FormComprovacaoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: FormComprovacaoComponent, selectors: [["app-form-comprovacao"]], decls: 7, vars: 2, consts: [[1, "buttons"], ["pButton", "", "pRipple", "", "type", "button", "label", "SALVAR", 3, "click"], ["pButton", "", "pRipple", "", "type", "button", "label", "CANCELAR", 1, "espacoEsqueda", 3, "click"], [1, "espacoRow", "min"], ["for", "textoToipico", 1, "label"], ["id", "textoToipico", "autoResize", "false", "maxlength", "500", "pInputTextarea", "", 3, "rows", "ngModel", "ngModelChange"]], template: function FormComprovacaoComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FormComprovacaoComponent_Template_button_click_1_listener() { return ctx.confirme(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FormComprovacaoComponent_Template_button_click_2_listener() { return ctx.close(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "label", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "NOVA COMPROVA\u00C7\u00C3O");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "textarea", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function FormComprovacaoComponent_Template_textarea_ngModelChange_6_listener($event) { return ctx.comprovacao = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rows", 5)("ngModel", ctx.comprovacao);
    } }, directives: [primeng_button__WEBPACK_IMPORTED_MODULE_2__["ButtonDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["MaxLengthValidator"], primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_4__["InputTextarea"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgModel"]], styles: [".espacoEsqueda[_ngcontent-%COMP%] {\n  margin-left: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2Zvcm0tY29tcHJvdmFjYW8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtBQUNKIiwiZmlsZSI6ImZvcm0tY29tcHJvdmFjYW8uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXNwYWNvRXNxdWVkYSB7XG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcbn0iXX0= */"] });


/***/ }),

/***/ "NXVx":
/*!********************************************************!*\
  !*** ./src/app/impedimentos/impedimentos.component.ts ***!
  \********************************************************/
/*! exports provided: ImpedimentosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImpedimentosComponent", function() { return ImpedimentosComponent; });
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var _form_impedimento_form_impedimento_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form-impedimento/form-impedimento.component */ "Cxho");
/* harmony import */ var _excluir_excluir_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../excluir/excluir.component */ "pJC4");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/table */ "rEr+");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/api */ "7zfz");









function ImpedimentosComponent_ng_template_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "th");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, "#");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "th", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](4, "IMPEDIMENTO (TEXTO OU LINK)");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](5, "th");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ImpedimentosComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "td", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "td", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "td", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "button", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ImpedimentosComponent_ng_template_7_Template_button_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r3.excluir(ctx_r3.item, "IMPEDIMENTO"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const impedimento_r2 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](impedimento_r2.id);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](impedimento_r2.texto);
} }
class ImpedimentosComponent {
    constructor(router, dialogService) {
        this.router = router;
        this.dialogService = dialogService;
        this.lista = [];
    }
    ngOnInit() {
        this.getLista();
    }
    getLista() {
        let impedimento = {
            id: 1,
            texto: 'O orçamento LOA ainda não foi aprovado',
        };
        this.lista.push(impedimento);
        let index = 2;
        impedimento =
            {
                id: index,
                texto: 'Não há pespectiva de disponibilidade financeira até o final do ano 2021',
            };
        index++;
        this.lista.push(impedimento);
        impedimento =
            {
                id: index,
                texto: 'https://ifpb.edu.br',
            };
        this.lista.push(impedimento);
    }
    show(titulo) {
        const ref = this.dialogService.open(_form_impedimento_form_impedimento_component__WEBPACK_IMPORTED_MODULE_1__["FormImpedimentoComponent"], {
            header: titulo,
            width: '100%',
            style: { "min-width": "720px", },
        });
        ref.onClose.subscribe(resposta => {
            if (resposta == 'confirm') {
                console.log('Confirmado.');
            }
        });
    }
    excluir(item, nomeitem) {
        const ref = this.dialogService.open(_excluir_excluir_component__WEBPACK_IMPORTED_MODULE_2__["ExcluirComponent"], {
            header: 'CONFIRMAR REMOÇÃO DO ' + nomeitem,
            data: {
                item: nomeitem,
            },
            width: '100%',
            // height: '100%',
            style: { "min-width": "720px", },
        });
        ref.onClose.subscribe(resposta => {
            if (resposta == 'confirm') {
                console.log('Confirmado para exclusão.');
            }
        });
    }
    click(url) {
        this.router.navigate([url]);
    }
}
ImpedimentosComponent.ɵfac = function ImpedimentosComponent_Factory(t) { return new (t || ImpedimentosComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__["DialogService"])); };
ImpedimentosComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: ImpedimentosComponent, selectors: [["app-impedimentos"]], features: [_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵProvidersFeature"]([primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__["DialogService"]])], decls: 8, vars: 2, consts: [[1, "btnAdicionar"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-plus", "title", "Adiconar impedimento \u00E0 a\u00E7\u00E3o de melhoria", 1, "p-button-rounded", 3, "click"], [1, "botoesTopContent"], ["pButton", "", "pRipple", "", "type", "button", "label", "VOLTAR", 1, "espacoEsqueda", 3, "click"], [1, "card", 2, "margin-top", "50px"], ["scrollHeight", "500px", 3, "value", "scrollable"], ["pTemplate", "header"], ["pTemplate", "body"], [2, "width", "80%"], [2, "width", "25px"], ["pButton", "", "type", "button", "title", "Excluir impedimento", "icon", "bi bi-x", "iconPos", "left", "styleClass", "iconBotao", 1, "p-button-danger", 3, "click"]], template: function ImpedimentosComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ImpedimentosComponent_Template_button_click_1_listener() { return ctx.show("ADICIONAR IMPEDIMENTOS"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ImpedimentosComponent_Template_button_click_3_listener() { return ctx.click("/acoesmelhorias"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "p-table", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](6, ImpedimentosComponent_ng_template_6_Template, 6, 0, "ng-template", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](7, ImpedimentosComponent_ng_template_7_Template, 7, 2, "ng-template", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("value", ctx.lista)("scrollable", true);
    } }, directives: [primeng_button__WEBPACK_IMPORTED_MODULE_5__["ButtonDirective"], primeng_table__WEBPACK_IMPORTED_MODULE_6__["Table"], primeng_api__WEBPACK_IMPORTED_MODULE_7__["PrimeTemplate"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJpbXBlZGltZW50b3MuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "Oy4E":
/*!*******************************************************!*\
  !*** ./src/app/dashboard/dashboard-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard.component */ "QX6l");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");




const routes = [
    { path: 'dashboard',
        component: _dashboard_component__WEBPACK_IMPORTED_MODULE_1__["DashboardComponent"],
        data: {
            title: 'Dashboard',
            breadcrumb: 'Dashboard'
        }
    },
];
class DashboardRoutingModule {
}
DashboardRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: DashboardRoutingModule });
DashboardRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ factory: function DashboardRoutingModule_Factory(t) { return new (t || DashboardRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](DashboardRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "QX6l":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class DashboardComponent {
    constructor() { }
    ngOnInit() {
    }
}
DashboardComponent.ɵfac = function DashboardComponent_Factory(t) { return new (t || DashboardComponent)(); };
DashboardComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: DashboardComponent, selectors: [["app-dashboard"]], decls: 2, vars: 0, template: function DashboardComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "dashboard works!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJkYXNoYm9hcmQuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "R69Q":
/*!**********************************************!*\
  !*** ./src/app/topicos/topicos.component.ts ***!
  \**********************************************/
/*! exports provided: TopicosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopicosComponent", function() { return TopicosComponent; });
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var _form_topico_form_topico_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form-topico/form-topico.component */ "fzUc");
/* harmony import */ var _excluir_excluir_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../excluir/excluir.component */ "pJC4");
/* harmony import */ var _core_GAS__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/GAS */ "Y7Y+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_dataview__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/dataview */ "8CEF");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var primeng_multiselect__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/multiselect */ "lVkt");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/dropdown */ "arFO");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/common */ "ofXK");

















function TopicosComponent_ng_template_4_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "label", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, "EIXOS / DIMENS\u00D5ES");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "p-multiSelect", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function TopicosComponent_ng_template_4_Template_p_multiSelect_ngModelChange_3_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r3.selectedDimensoes = $event; })("onChange", function TopicosComponent_ng_template_4_Template_p_multiSelect_onChange_3_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r5.filtra($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "span", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "label", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](6, "NIVEL");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "p-dropdown", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function TopicosComponent_ng_template_4_Template_p_dropdown_ngModelChange_7_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r6.selectedNivel = $event; })("onChange", function TopicosComponent_ng_template_4_Template_p_dropdown_onChange_7_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r7.filtra($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](8, "span", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "label", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](10, "SEGMENTOS");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](11, "p-multiSelect", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function TopicosComponent_ng_template_4_Template_p_multiSelect_ngModelChange_11_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r8.selectedSegmentos = $event; })("onChange", function TopicosComponent_ng_template_4_Template_p_multiSelect_onChange_11_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r9.filtra($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](12, "span", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](13, "label", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](14, "SEMTIMENTO");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](15, "p-dropdown", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function TopicosComponent_ng_template_4_Template_p_dropdown_ngModelChange_15_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r10.selectedSentimento = $event; })("onChange", function TopicosComponent_ng_template_4_Template_p_dropdown_onChange_15_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r11.filtra($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](16, "span", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](17, "label", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](18, "TIPO");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](19, "p-dropdown", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function TopicosComponent_ng_template_4_Template_p_dropdown_ngModelChange_19_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r12.selectedTipo = $event; })("onChange", function TopicosComponent_ng_template_4_Template_p_dropdown_onChange_19_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r13.filtra($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](20, "span", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](21, "label", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](22, "A\u00C7\u00D5ES");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](23, "p-dropdown", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function TopicosComponent_ng_template_4_Template_p_dropdown_ngModelChange_23_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r14.selectedAcao = $event; })("onChange", function TopicosComponent_ng_template_4_Template_p_dropdown_onChange_23_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r15.filtra($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](24, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](25, "i", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](26, "input", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("input", function TopicosComponent_ng_template_4_Template_input_input_26_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](3); return _r0.filter($event.target.value); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("options", ctx_r1.dimensoes)("ngModel", ctx_r1.selectedDimensoes);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("options", ctx_r1.niveis)("ngModel", ctx_r1.selectedNivel);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("options", ctx_r1.segmentos)("ngModel", ctx_r1.selectedSegmentos);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("options", ctx_r1.sentimentos)("ngModel", ctx_r1.selectedSentimento);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("options", ctx_r1.tipos)("ngModel", ctx_r1.selectedTipo);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("options", ctx_r1.acoes)("ngModel", ctx_r1.selectedAcao);
} }
function TopicosComponent_ng_template_5_ng_template_1_div_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](0, "div");
} }
function TopicosComponent_ng_template_5_ng_template_1_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "img", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function TopicosComponent_ng_template_5_ng_template_1_ng_template_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "img", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function TopicosComponent_ng_template_5_ng_template_1_span_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const segmento_r26 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" \u2630", segmento_r26, " ");
} }
function TopicosComponent_ng_template_5_ng_template_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "table", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "td", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "span", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "td", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](6, TopicosComponent_ng_template_5_ng_template_1_div_6_Template, 1, 0, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](7, TopicosComponent_ng_template_5_ng_template_1_ng_template_7_Template, 2, 0, "ng-template", null, 26, _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](9, TopicosComponent_ng_template_5_ng_template_1_ng_template_9_Template, 2, 0, "ng-template", null, 27, _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](11, "span", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](12, " T\u00D3PICO ");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](13, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](14, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](15, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](17, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](18, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](19, TopicosComponent_ng_template_5_ng_template_1_span_19_Template, 2, 1, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](20, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](21, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](22, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](23);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](8);
    const _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](10);
    const item_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" \u2630", item_r17.dimensao, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", item_r17.sentimento == "POTENCIALIDADE")("ngIfThen", _r23)("ngIfElse", _r21);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" \u2630", item_r17.nivelAplicavel, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngForOf", item_r17.segmentos);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" \u2630", item_r17.tipoTopico, " ");
} }
function TopicosComponent_ng_template_5_ng_template_5_Template(rf, ctx) { if (rf & 1) {
    const _r30 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "table", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "td", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](11, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](13, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](14);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](15, "td", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](16, "p-button", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function TopicosComponent_ng_template_5_ng_template_5_Template_p_button_click_16_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r30); const item_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit; const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r28.show("EDITAR T\u00D3PICO", item_r17); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](17, "p-button", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function TopicosComponent_ng_template_5_ng_template_5_Template_p_button_click_17_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r30); const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](2); return ctx_r31.click("/acoesmelhorias"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](18, "p-button", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function TopicosComponent_ng_template_5_ng_template_5_Template_p_button_click_18_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r30); const item_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit; const ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r32.excluir(item_r17, "T\u00D3PICO"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    const ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵclassMapInterpolate1"]("totaliza ", ctx_r19.cor.bg, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"]("", item_r17.naoAvaliada, " N\u00C3O AVALIADA");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵclassMapInterpolate1"]("totaliza ", ctx_r19.cor.bg, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"]("", item_r17.debatidaEplanejada, " DEBATIDA E PLANEJADA");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵclassMapInterpolate1"]("totaliza ", ctx_r19.cor.bg, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"]("", item_r17.indicada, " INICIADA");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵclassMapInterpolate1"]("totaliza ", ctx_r19.cor.bg, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"]("", item_r17.implantadaParcialmente, " IMPLANTADA PARCIALMENTE ");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵclassMapInterpolate1"]("totaliza ", ctx_r19.cor.bg, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"]("", item_r17.implantadaTotalmente, " IMPLANTADA TOTALMENTE");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate2"]("", item_r17.emailUltimoRevisor, " (", item_r17.dataUltimaRevisao, ")");
} }
function TopicosComponent_ng_template_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "p-card");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, TopicosComponent_ng_template_5_ng_template_1_Template, 24, 7, "ng-template", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](5, TopicosComponent_ng_template_5_ng_template_5_Template, 19, 22, "ng-template", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r17 = ctx.$implicit;
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", ctx_r2.verificaCor(item_r17.sentimento), " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵclassMapInterpolate1"]("bodyTopico ", ctx_r2.cor.texto, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", item_r17.topicoOuIndicador, " ");
} }
class TopicosComponent {
    constructor(router, route, dialogService, gas) {
        this.router = router;
        this.route = route;
        this.dialogService = dialogService;
        this.gas = gas;
        this.niveis = [];
        this.segmentos = [];
        this.dimensoes = [];
        this.sentimentos = [];
        this.tipos = [];
        this.acoes = [];
        this.cor = { texto: 'TXfragilidade', bg: 'BGfragilidade' };
        // cor = {texto: 'TXpotencialidade', bg:'BGpotencialidade'}
        this.lista = [];
        this.listaAux = [];
    }
    ngOnInit() {
        this.getParametos();
    }
    /**
     * Ler parametro passana url (id painel apropriador)
     */
    getParametos() {
        this.route.queryParams.subscribe(params => {
            if (params['painel'] != undefined) {
                this.painelApropriador = params['painel'];
                this.getLista(this.painelApropriador);
            }
            ;
        });
    }
    /**
     * Busca tópicos do painel apropriador
     * @param painel ID do painel
     */
    getLista(painel) {
        _core_GAS__WEBPACK_IMPORTED_MODULE_3__["REQUESTGAS"].functionName = _core_GAS__WEBPACK_IMPORTED_MODULE_3__["RESOURCE_TOPICOS_GETListarTodos"].functionName;
        _core_GAS__WEBPACK_IMPORTED_MODULE_3__["REQUESTGAS"].functionParams = [];
        this.gas.GASRequest(_core_GAS__WEBPACK_IMPORTED_MODULE_3__["REQUESTGAS"], this.resultadoTopicos, this);
        // this.resultadoTopicos('fake');
    }
    resultadoTopicos(response) {
        console.log('Resposta');
        console.log(response);
        this.tratarResposta(response);
    }
    tratarResposta(response) {
        // let listaTopicos = response; 
        // listatopicos fake
        let listaTopicos = this.fake();
        this.listaAux = listaTopicos;
        this.lista = listaTopicos;
        this.getNiveis(this.lista);
        this.getSegmentos(this.lista);
        this.getDimensoes(this.lista);
        this.getSentimento(this.lista);
        this.getTipos(this.lista);
        this.getAcoes(this.lista);
    }
    fake() {
        return [
            {
                id: 1,
                dimensao: 'Dimensão 8: Planejamento e Avaliação',
                painelAprorpiador: { id: this.painelApropriador },
                emailUltimoRevisor: 'giuseppe.lima@ifpb.edu.br',
                dataUltimaRevisao: '20/02/2021 12:45:38',
                segmentos: ['TÉCNICO ADMINISTRATIVO', 'GESTOR'],
                nivelAplicavel: 'CAMPUS',
                tipoTopico: 'REIVINDICAÇÃO',
                topicoOuIndicador: 'lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo le lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo d lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem.',
                sentimento: 'POTENCIALIDADE',
                naoAvaliada: 23,
                debatidaEplanejada: 10,
                indicada: 10,
                implantadaParcialmente: 10,
                implantadaTotalmente: 10,
            },
            {
                id: 2,
                dimensao: 'Dimensão 2: Políticas para o Ensino, a Pesquisa e a Extensão',
                painelAprorpiador: { id: this.painelApropriador },
                emailUltimoRevisor: 'giuseppe.lima@ifpb.edu.br',
                dataUltimaRevisao: '20/02/2021 12:45:38',
                segmentos: ['DOCENTE', 'DISCENTE', 'GESTOR'],
                nivelAplicavel: 'CURSO',
                tipoTopico: 'AVALIAÇÃO INTERNA',
                topicoOuIndicador: 'lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo le lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo d lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem.',
                sentimento: 'FRAGILIDADE',
                naoAvaliada: 23,
                debatidaEplanejada: 10,
                indicada: 10,
                implantadaParcialmente: 10,
                implantadaTotalmente: 10,
            },
            {
                id: 3,
                dimensao: 'Dimensão 2: Políticas para o Ensino, a Pesquisa e a Extensão',
                painelAprorpiador: { id: this.painelApropriador },
                emailUltimoRevisor: 'giuseppe.lima@ifpb.edu.br',
                dataUltimaRevisao: '20/02/2021 12:45:38',
                segmentos: ['GESTOR'],
                nivelAplicavel: 'CAMPUS',
                tipoTopico: 'REIVINDICAÇÃO',
                topicoOuIndicador: 'lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo le lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo d lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem ipsum a lo lorem.',
                sentimento: 'POTENCIALIDADE',
                naoAvaliada: 23,
                debatidaEplanejada: 10,
                indicada: 10,
                implantadaParcialmente: 10,
                implantadaTotalmente: 10,
            },
        ];
    }
    verificaCor(sentimento) {
        if (sentimento == 'FRAGILIDADE')
            this.cor = { texto: 'TXfragilidade', bg: 'BGfragilidade' };
        else
            this.cor = { texto: 'TXpotencialidade', bg: 'BGpotencialidade' };
    }
    show(titulo, topico = null) {
        const ref = this.dialogService.open(_form_topico_form_topico_component__WEBPACK_IMPORTED_MODULE_1__["FormTopicoComponent"], {
            header: titulo,
            data: topico,
            width: '100%',
            height: '100%',
            style: { "min-width": "720px", },
        });
        ref.onClose.subscribe(resposta => {
            if (resposta == 'confirm') {
                console.log('Confirmado.');
            }
        });
    }
    excluir(item, nomeitem) {
        const ref = this.dialogService.open(_excluir_excluir_component__WEBPACK_IMPORTED_MODULE_2__["ExcluirComponent"], {
            header: 'CONFIRMAR REMOÇÃO DO ' + nomeitem,
            data: {
                item: nomeitem,
            },
            width: '100%',
            // height: '100%',
            style: { "min-width": "720px", },
        });
        ref.onClose.subscribe(resposta => {
            if (resposta == 'confirm') {
                console.log('Confirmado para exclusão.');
            }
        });
    }
    click(url) {
        this.router.navigate([url]);
    }
    getNiveis(lista) {
        this.niveis = [];
        this.niveis.push({ nivel: 'TODOS' });
        lista.forEach(element => {
            let index = this.niveis.findIndex(val => val.nivel == element.nivelAplicavel);
            if (index < 0) {
                this.niveis.push({ nivel: element.nivelAplicavel });
            }
        });
    }
    getSegmentos(lista) {
        this.segmentos = [];
        lista.forEach(element => {
            element.segmentos.forEach(element => {
                // Verifica se já existe o segmento
                let index = this.segmentos.findIndex(val => val.segmento == element);
                // Se ainda não adicionado, adicionar para seleção.
                if (index < 0) {
                    this.segmentos.push({ segmento: element });
                }
            });
        });
    }
    getDimensoes(lista) {
        this.dimensoes = [];
        lista.forEach(element => {
            let index = this.dimensoes.findIndex(val => val.dimensoes == element.dimensao);
            if (index < 0) {
                this.dimensoes.push({ dimensoes: element.dimensao });
            }
        });
    }
    getSentimento(lista) {
        this.sentimentos = [];
        this.sentimentos.push({ sentimento: 'TODOS' });
        lista.forEach(element => {
            let index = this.sentimentos.findIndex(val => val.sentimento == element.sentimento);
            if (index < 0) {
                this.sentimentos.push({ sentimento: element.sentimento });
            }
        });
    }
    getTipos(lista) {
        this.tipos = [];
        this.tipos.push({ tipo: 'TODOS' });
        lista.forEach(element => {
            let index = this.tipos.findIndex(val => val.tipo == element.tipoTopico);
            if (index < 0) {
                this.tipos.push({ tipo: element.tipoTopico });
            }
        });
    }
    getAcoes(lista) {
        this.acoes.push({ acao: 'TODOS' });
        this.acoes.push({ acao: 'SIM' });
        this.acoes.push({ acao: 'NÃO' });
    }
    /**
    * Filta pelos campos de filtro utiliza uma lista auxiliar para não perder o getLista inicial
    * E atualiza os outro campos de filtro apenas para conter informaçoes do que foi filtrado
    * @param event
    */
    filtra(event) {
        this.lista = this.listaAux;
        // Filtro por mult dimensões 
        if (this.selectedDimensoes != undefined && this.selectedDimensoes.length > 0) {
            let filtro = this.selectedDimensoes.map(item => { return item.dimensoes; });
            this.lista = this.lista.filter(key => filtro.includes(key.dimensao));
        }
        // Filtra por nivel
        if (this.selectedNivel != undefined) {
            let nivel = this.selectedNivel.nivel;
            let filtro = this.niveis.map(item => { return item.nivel; });
            if (nivel == 'TODOS') {
                this.lista = this.lista.filter(key => filtro.includes(key.nivelAplicavel));
            }
            else {
                this.lista = this.lista.filter(function (key) {
                    return key.nivelAplicavel === nivel;
                });
            }
        }
        // Filtra por mult segmento
        if (this.selectedSegmentos != undefined && this.selectedSegmentos.length > 0) {
            let filtro = this.selectedSegmentos.map(item => { return item.segmento; });
            let novaLista = [];
            this.lista.forEach(element => {
                let tem = element.segmentos.filter(key => filtro.includes(key));
                if (tem.length > 0)
                    novaLista.push(element);
            });
            this.lista = novaLista;
        }
        // Filtra por sentimento
        if (this.selectedSentimento != undefined) {
            let sentimento = this.selectedSentimento.sentimento;
            let sentimentoFiltro = this.sentimentos.map(item => { return item.sentimento; });
            if (sentimento == 'TODOS') {
                this.lista = this.lista.filter(key => sentimentoFiltro.includes(key.sentimento));
            }
            else {
                this.lista = this.lista.filter(function (key) {
                    return key.sentimento === sentimento;
                });
            }
        }
        // Filtra por tipo
        if (this.selectedTipo != undefined) {
            let tipo = this.selectedTipo.tipo;
            let filtro = this.tipos.map(item => { return item.tipo; });
            if (tipo == 'TODOS') {
                this.lista = this.lista.filter(key => filtro.includes(key.tipoTopico));
            }
            else {
                this.lista = this.lista.filter(function (key) {
                    return key.tipoTopico === tipo;
                });
            }
        }
        // Filtro por ação a definir
    }
}
TopicosComponent.ɵfac = function TopicosComponent_Factory(t) { return new (t || TopicosComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_core_GAS__WEBPACK_IMPORTED_MODULE_3__["GAS"])); };
TopicosComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({ type: TopicosComponent, selectors: [["app-topicos"]], features: [_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵProvidersFeature"]([primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__["DialogService"]])], decls: 6, vars: 3, consts: [[1, "btnAdicionar"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-plus", "title", "Adiconar t\u00F3pico", 1, "p-button-rounded", 3, "click"], ["paginatorPosition", "both", "filterBy", "ciclo", "layout", "grid", 3, "value", "paginator", "rows"], ["dv", ""], ["pTemplate", "header"], ["pTemplate", "gridItem"], [1, "block"], ["for", "dropdownEixoDimensoes", 1, "label"], ["id", "dropdownEixoDimensoes", "defaultLabel", "TODOS", "optionLabel", "dimensoes", 3, "options", "ngModel", "ngModelChange", "onChange"], ["for", "dropdownNivel", 1, "label"], ["id", "dropdownNivel", "optionLabel", "nivel", 3, "options", "ngModel", "ngModelChange", "onChange"], ["for", "dropdownSegmentos", 1, "label"], ["id", "dropdownSegmentos", "defaultLabel", "TODOS", "optionLabel", "segmento", 3, "options", "ngModel", "ngModelChange", "onChange"], ["for", "dropdownCampus", 1, "label"], ["id", "dropdownCampus", "optionLabel", "sentimento", 3, "options", "ngModel", "ngModelChange", "onChange"], ["id", "dropdownCampus", "optionLabel", "tipo", 3, "options", "ngModel", "ngModelChange", "onChange"], ["id", "dropdownCampus", "optionLabel", "acao", 3, "options", "ngModel", "ngModelChange", "onChange"], [1, "p-input-icon-left"], [1, "pi", "pi-search"], ["type", "search", "pInputText", "", "placeholder", "Pesquisar", 3, "input"], ["pTemplate", "footer"], [1, "tabela"], [1, "col80"], [1, "tagTitulo"], ["rowspan", "3"], [4, "ngIf", "ngIfThen", "ngIfElse"], ["fragilidadeBlock", ""], ["potencialidadeBlock", ""], [1, "tituloCard", "down"], [1, "tagSubtitulo"], ["class", "tagSubtitulo", 4, "ngFor", "ngForOf"], [1, "tituloCard"], ["src", "../../assets/icons/faceVermelha.svg"], ["src", "../../assets/icons/faceAzul.svg"], [1, "revisado"], [1, "botoesCard"], ["icon", "bi bi-gear-fill", "iconPos", "left", "title", "Editar t\u00F3pico", "styleClass", "iconBotao", 3, "click"], ["icon", "bi bi-lightbulb-fill", "iconPos", "left", "title", "A\u00E7\u00F5es de melhorias do t\u00F3picos", "styleClass", "iconBotao", 1, "espacoEsqueda", 3, "click"], ["icon", "bi bi-trash-fill", "iconPos", "left", "styleClass", "iconBotao", "title", "Excluir t\u00F3picos", 1, "espacoEsqueda", 3, "click"]], template: function TopicosComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function TopicosComponent_Template_button_click_1_listener() { return ctx.show("ADICIONAR T\u00D3PICO"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "p-dataView", 2, 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](4, TopicosComponent_ng_template_4_Template, 27, 12, "ng-template", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](5, TopicosComponent_ng_template_5_Template, 6, 5, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("value", ctx.lista)("paginator", true)("rows", 9);
    } }, directives: [primeng_button__WEBPACK_IMPORTED_MODULE_6__["ButtonDirective"], primeng_dataview__WEBPACK_IMPORTED_MODULE_7__["DataView"], primeng_api__WEBPACK_IMPORTED_MODULE_8__["PrimeTemplate"], primeng_multiselect__WEBPACK_IMPORTED_MODULE_9__["MultiSelect"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgModel"], primeng_dropdown__WEBPACK_IMPORTED_MODULE_11__["Dropdown"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_12__["InputText"], primeng_card__WEBPACK_IMPORTED_MODULE_13__["Card"], _angular_common__WEBPACK_IMPORTED_MODULE_14__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_14__["NgForOf"], primeng_button__WEBPACK_IMPORTED_MODULE_6__["Button"]], styles: [".totaliza[_ngcontent-%COMP%] {\n  font-size: 10px;\n  font-weight: 600;\n  padding: 5px 3px 3px 3px;\n  color: #ffffff;\n  text-align: left;\n  margin: 0px 0px 5px 5px;\n  float: left;\n}\n\n.BGfragilidade[_ngcontent-%COMP%] {\n  background-color: #d9001b;\n}\n\n.TXfragilidade[_ngcontent-%COMP%] {\n  color: #d9001b;\n}\n\n.BGpotencialidade[_ngcontent-%COMP%] {\n  background-color: #02a7f0;\n}\n\n.TXpotencialidade[_ngcontent-%COMP%] {\n  color: #02a7f0;\n}\n\n[_nghost-%COMP%]     .iconBotao {\n  font-size: 1.5rem !important;\n  padding-bottom: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3RvcGljb3MuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFBO0VBRUEsZ0JBQUE7RUFDQSx3QkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLHVCQUFBO0VBQ0EsV0FBQTtBQUFKOztBQUdBO0VBQ0kseUJBQUE7QUFBSjs7QUFFQTtFQUNJLGNBQUE7QUFDSjs7QUFDQTtFQUNJLHlCQUFBO0FBRUo7O0FBQUE7RUFDSSxjQUFBO0FBR0o7O0FBR0k7RUFDSSw0QkFBQTtFQUNBLG1CQUFBO0FBQVIiLCJmaWxlIjoidG9waWNvcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50b3RhbGl6YSB7XG4gICAgZm9udC1zaXplOiAxMHB4O1xuICAgIC8vIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgcGFkZGluZzogNXB4IDNweCAzcHggM3B4O1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgbWFyZ2luOiAwcHggMHB4IDVweCA1cHg7XG4gICAgZmxvYXQ6IGxlZnQ7XG59XG5cbi5CR2ZyYWdpbGlkYWRle1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkOTAwMWI7XG59XG4uVFhmcmFnaWxpZGFkZXtcbiAgICBjb2xvcjogI2Q5MDAxYjtcbn1cbi5CR3BvdGVuY2lhbGlkYWRle1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMmE3ZjA7XG59XG4uVFhwb3RlbmNpYWxpZGFkZXtcbiAgICBjb2xvcjogIzAyYTdmMDtcbn1cblxuXG5cbjpob3N0IHtcbiAgICA6Om5nLWRlZXAgLmljb25Cb3RhbyB7XG4gICAgICAgIGZvbnQtc2l6ZTogMS41cmVtICFpbXBvcnRhbnQ7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gICAgfSAgICBcbn0iXX0= */"] });


/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _core_loading__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./core/loading */ "xhfB");
/* harmony import */ var _menu_menu_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./menu/menu.component */ "TGkX");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");




class AppComponent {
    constructor(loading) {
        this.loading = loading;
        this.title = 'avin-ui';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_core_loading__WEBPACK_IMPORTED_MODULE_1__["Loading"])); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 2, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-menu");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "router-outlet");
    } }, directives: [_menu_menu_component__WEBPACK_IMPORTED_MODULE_2__["MenuComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "TDBs":
/*!***********************************************!*\
  !*** ./src/app/dashboard/dashboard.module.ts ***!
  \***********************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard.component */ "QX6l");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard-routing.module */ "Oy4E");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




class DashboardModule {
}
DashboardModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({ type: DashboardModule });
DashboardModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({ factory: function DashboardModule_Factory(t) { return new (t || DashboardModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_2__["DashboardRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](DashboardModule, { declarations: [_dashboard_component__WEBPACK_IMPORTED_MODULE_1__["DashboardComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_2__["DashboardRoutingModule"]], exports: [_dashboard_component__WEBPACK_IMPORTED_MODULE_1__["DashboardComponent"]] }); })();


/***/ }),

/***/ "TGkX":
/*!****************************************!*\
  !*** ./src/app/menu/menu.component.ts ***!
  \****************************************/
/*! exports provided: MenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuComponent", function() { return MenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/toolbar */ "5EWq");
/* harmony import */ var _breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../breadcrumb/breadcrumb.component */ "55oq");
/* harmony import */ var primeng_sidebar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/sidebar */ "jLSX");





class MenuComponent {
    constructor(router) {
        this.router = router;
        this.visibleSidebar = false;
    }
    ngOnInit() {
    }
    visibleMenu() {
        this.visibleSidebar = !this.visibleSidebar;
    }
    sair() {
    }
    click(url) {
        this.router.navigate([url]);
        this.visibleMenu();
    }
    videoAjuda() {
    }
}
MenuComponent.ɵfac = function MenuComponent_Factory(t) { return new (t || MenuComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"])); };
MenuComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: MenuComponent, selectors: [["app-menu"]], decls: 22, vars: 4, consts: [[1, "ui-toolbar-group-left"], ["id", "menu", 3, "click"], [1, "pi", "pi-bars", "icon_options"], [1, "center", "div_logo"], ["src", "../../../assets/img/logoIfpb.png", 1, "logoIFPB"], ["src", "../../../assets/img/logoAvin.png", 1, "logoAvin"], [1, "ui-toolbar-group-right"], [3, "click"], [1, "pi", "pi-question-circle", "icon_options", "espacoDireita"], ["id", "btnSair", "title", "Sair", 3, "click"], [1, "pi", "pi-sign-out", "icon_options"], ["styleClass", "my-sidebar", 3, "visible", "baseZIndex", "showCloseIcon", "modal", "visibleChange"], [1, "item_menu", 3, "click"], [1, "item_menu_vertical"], ["id", "Dashboard"]], template: function MenuComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-toolbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MenuComponent_Template_a_click_2_listener() { return ctx.visibleMenu(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "i", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "img", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MenuComponent_Template_a_click_8_listener() { return ctx.videoAjuda(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "i", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MenuComponent_Template_a_click_10_listener() { return ctx.sair(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "app-breadcrumb");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p-sidebar", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function MenuComponent_Template_p_sidebar_visibleChange_13_listener($event) { return ctx.visibleSidebar = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MenuComponent_Template_div_click_14_listener() { ctx.click("/dashboard"); ctx.visibleMenu(); return ctx.visibleMenu(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "span", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Dashboard");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MenuComponent_Template_div_click_18_listener() { ctx.click("/painelapropriador"); ctx.visibleMenu(); return ctx.visibleMenu(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "span", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Painel Apropriador");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.visibleSidebar)("baseZIndex", 10000)("showCloseIcon", false)("modal", false);
    } }, directives: [primeng_toolbar__WEBPACK_IMPORTED_MODULE_2__["Toolbar"], _breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbComponent"], primeng_sidebar__WEBPACK_IMPORTED_MODULE_4__["Sidebar"]], styles: [".logoAvin[_ngcontent-%COMP%] {\n  height: 50px;\n}\n\n.logoIFPB[_ngcontent-%COMP%] {\n  height: 50px;\n}\n\n.icon_options[_ngcontent-%COMP%] {\n  cursor: pointer;\n  font-size: 30px;\n  color: black;\n}\n\n.center[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n.espacoDireita[_ngcontent-%COMP%] {\n  margin-right: 10px;\n}\n\n[_nghost-%COMP%]     .my-sidebar {\n  top: 4.7em;\n  height: 80px;\n  width: auto;\n  padding: 0px;\n  background-color: #333333;\n  font-weight: 400;\n  font-style: normal;\n  text-align: left;\n  color: #FFFFFF;\n}\n\n.item_menu[_ngcontent-%COMP%] {\n  cursor: pointer;\n  height: 35px;\n  padding: 20px;\n  padding-left: 40px;\n  font-family: -apple-system, BlinkMacSystemFont, \"Segoe UI\", Roboto, Helvetica, Arial, sans-serif, \"Apple Color Emoji\", \"Segoe UI Emoji\", \"Segoe UI Symbol\";\n}\n\n.item_menu_vertical[_ngcontent-%COMP%] {\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n.item_menu[_ngcontent-%COMP%]:hover {\n  background-color: #00a650;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL21lbnUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFRSxZQUFBO0FBQUY7O0FBR0E7RUFFRSxZQUFBO0FBREY7O0FBSUE7RUFDRSxlQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7QUFERjs7QUFJQTtFQUNFLGtCQUFBO0FBREY7O0FBVUE7RUFDRSxrQkFBQTtBQVBGOztBQVlFO0VBQ0UsVUFBQTtFQUVBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBRUEsZ0JBQUE7RUFDQSxrQkFBQTtFQUVBLGdCQUFBO0VBQ0EsY0FBQTtBQVpKOztBQWdCQTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsMEpBQUE7QUFiRjs7QUFnQkE7RUFDRSxRQUFBO0VBRUEsMkJBQUE7QUFiRjs7QUFnQkE7RUFDRSx5QkFBQTtBQWJGIiwiZmlsZSI6Im1lbnUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9nb0F2aW4ge1xuICAvLyB3aWR0aDogODdweDtcbiAgaGVpZ2h0OiA1MHB4O1xufVxuXG4ubG9nb0lGUEIge1xuICAvLyB3aWR0aDogMzNweDtcbiAgaGVpZ2h0OiA1MHB4O1xufVxuXG4uaWNvbl9vcHRpb25zIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBmb250LXNpemU6IDMwcHg7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLmNlbnRlciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmRpdl9sb2dvIHtcbiAgLy8gcG9zaXRpb246IGFic29sdXRlO1xuICAvLyBsZWZ0OiA1MCU7XG4gIC8vIG1hcmdpbi1sZWZ0OiAtNjBweDsgLyogZSByZXRyb2NlZGUgbWV0YWRlIGRhIGxhcmd1cmEgKi9cbn1cblxuLmVzcGFjb0RpcmVpdGEge1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi8vIFJlZXNjcmV2ZW5kbyB0aGVtYVxuOmhvc3Qge1xuICA6Om5nLWRlZXAgLm15LXNpZGViYXIge1xuICAgIHRvcDogNC43ZW07XG4gICAgLy8gaGVpZ2h0IGRvIHNpZGViYXIgNDAgcG9yIGl0ZW0gYWRpY2lvbmFkb1xuICAgIGhlaWdodDogODBweDtcbiAgICB3aWR0aDogYXV0bztcbiAgICBwYWRkaW5nOiAwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzMzMzMzMztcbiAgICAvLyBmb250LWZhbWlseTogJ09wZW4gU2Fucyc7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgLy8gZm9udC1zaXplOiAxNHB4O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7IFxuICAgIGNvbG9yOiAjRkZGRkZGO1xuICB9XG59XG5cbi5pdGVtX21lbnUge1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGhlaWdodDogMzVweDtcbiAgcGFkZGluZzogMjBweDtcbiAgcGFkZGluZy1sZWZ0OiA0MHB4O1xuICBmb250LWZhbWlseTogLWFwcGxlLXN5c3RlbSwgQmxpbmtNYWNTeXN0ZW1Gb250LCBcIlNlZ29lIFVJXCIsIFJvYm90bywgSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZiwgXCJBcHBsZSBDb2xvciBFbW9qaVwiLCBcIlNlZ29lIFVJIEVtb2ppXCIsIFwiU2Vnb2UgVUkgU3ltYm9sXCJcbn1cblxuLml0ZW1fbWVudV92ZXJ0aWNhbCB7XG4gIHRvcDogNTAlO1xuICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG5cbi5pdGVtX21lbnU6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDE2NiwgODAsIDEpO1xufVxuIl19 */"] });


/***/ }),

/***/ "VLMe":
/*!***************************************************************************************!*\
  !*** ./src/app/acoes-de-melhorias/form-acao-melhoria/form-acao-melhoria.component.ts ***!
  \***************************************************************************************/
/*! exports provided: FormAcaoMelhoriaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormAcaoMelhoriaComponent", function() { return FormAcaoMelhoriaComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/inputtextarea */ "zFJ7");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/dropdown */ "arFO");






class FormAcaoMelhoriaComponent {
    constructor(ref, config) {
        this.ref = ref;
        this.config = config;
        this.escalas = [];
    }
    ngOnInit() {
        this.getEscalaAvaliativa();
    }
    close() {
        this.ref.close();
    }
    confirme() {
        this.ref.close('confirm');
    }
    getEscalaAvaliativa() {
        this.escalas.push({ escala: 'NÃO AVALIADA' });
        this.escalas.push({ escala: 'DEBATIDA E PLANEJADA' });
        this.escalas.push({ escala: 'INICIADA' });
        this.escalas.push({ escala: 'IMPLANTADA PARCIALMENTE' });
        this.escalas.push({ escala: 'IMPLANTADA TOTALMENTE' });
    }
}
FormAcaoMelhoriaComponent.ɵfac = function FormAcaoMelhoriaComponent_Factory(t) { return new (t || FormAcaoMelhoriaComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_1__["DynamicDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_1__["DynamicDialogConfig"])); };
FormAcaoMelhoriaComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: FormAcaoMelhoriaComponent, selectors: [["app-form-acao-melhoria"]], decls: 16, vars: 6, consts: [[1, "buttons"], ["pButton", "", "pRipple", "", "type", "button", "label", "SALVAR", 3, "click"], ["pButton", "", "pRipple", "", "type", "button", "label", "CANCELAR", 1, "espacoEsqueda", 3, "click"], [1, "espacoRow", "min"], ["for", "textoToipico", 1, "label"], ["id", "textoToipico", "autoResize", "false", "maxlength", "500", "pInputTextarea", "", 3, "rows", "ngModel", "ngModelChange"], [1, "block"], ["for", "dropdownEscala", 1, "label"], ["id", "dropdownEscala", "appendTo", "body", "optionLabel", "escala", 3, "options", "ngModel", "ngModelChange"]], template: function FormAcaoMelhoriaComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FormAcaoMelhoriaComponent_Template_button_click_1_listener() { return ctx.confirme(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FormAcaoMelhoriaComponent_Template_button_click_2_listener() { return ctx.close(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "label", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "QUAL");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "textarea", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function FormAcaoMelhoriaComponent_Template_textarea_ngModelChange_6_listener($event) { return ctx.qual = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "label", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "COMO");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "textarea", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function FormAcaoMelhoriaComponent_Template_textarea_ngModelChange_10_listener($event) { return ctx.como = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "span", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "label", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "ESCALA AVALIATIVA");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p-dropdown", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function FormAcaoMelhoriaComponent_Template_p_dropdown_ngModelChange_15_listener($event) { return ctx.selectedEscalas = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rows", 5)("ngModel", ctx.qual);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("rows", 5)("ngModel", ctx.como);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("options", ctx.escalas)("ngModel", ctx.selectedEscalas);
    } }, directives: [primeng_button__WEBPACK_IMPORTED_MODULE_2__["ButtonDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["MaxLengthValidator"], primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_4__["InputTextarea"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgModel"], primeng_dropdown__WEBPACK_IMPORTED_MODULE_5__["Dropdown"]], styles: [".espacoEsqueda[_ngcontent-%COMP%] {\n  margin-left: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2Zvcm0tYWNhby1tZWxob3JpYS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFBO0FBQ0oiLCJmaWxlIjoiZm9ybS1hY2FvLW1lbGhvcmlhLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmVzcGFjb0VzcXVlZGEge1xuICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG59Il19 */"] });


/***/ }),

/***/ "Y7Y+":
/*!*****************************!*\
  !*** ./src/app/core/GAS.ts ***!
  \*****************************/
/*! exports provided: GAS, REQUESTGAS, RESPONSE, RESOURCE_EIXO, RESOURCE_SEGMENTO, RESOURCE_NIVEISORGANIZACIONAIS, RESOURCE_PAINEL_APROPRIADOR, RESOURCE_TOPICOS_GETListarTodos, RESOURCE_ACAO_MELHORIA_GETListarTodos */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GAS", function() { return GAS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "REQUESTGAS", function() { return REQUESTGAS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESPONSE", function() { return RESPONSE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESOURCE_EIXO", function() { return RESOURCE_EIXO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESOURCE_SEGMENTO", function() { return RESOURCE_SEGMENTO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESOURCE_NIVEISORGANIZACIONAIS", function() { return RESOURCE_NIVEISORGANIZACIONAIS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESOURCE_PAINEL_APROPRIADOR", function() { return RESOURCE_PAINEL_APROPRIADOR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESOURCE_TOPICOS_GETListarTodos", function() { return RESOURCE_TOPICOS_GETListarTodos; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RESOURCE_ACAO_MELHORIA_GETListarTodos", function() { return RESOURCE_ACAO_MELHORIA_GETListarTodos; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

// Origem do evento a ser despachado pelo postMessage
const GAS_DOMAIN = "https://n-mwam3j73nfuaoxfsjm7wewgljkcxvqqzhi4jwwy-2lu-script.googleusercontent.com"; // equivale ao dominio da url da src do iframe pai
// 
class GAS {
    constructor() {
        window.addEventListener('message', (event) => {
            this.GASCallbackRun(event, this.modulo);
        });
    }
    /**
     * Faz requisição ao fram pai (Back-End)
     * @param requestObject     const REQUEST
     * @param functionCallbackName
     * @param modulo
     */
    GASRequest(requestObject, functionCallbackName, modulo) {
        let message = {
            // functionRunName: GASRequest,
            functionRunParams: requestObject,
            functionCallbackName: functionCallbackName.name
        };
        this.modulo = modulo;
        window.parent.postMessage(message, GAS_DOMAIN);
    }
    /**
     * Invocada via iframe pai, quando algo é retornado pelo fim de google.script.run lá na cloud
     * @param e   função a ser chamada no iframe filho
     * @param angularModuleObject parâmetros da função chamada no iframe filho
     */
    GASCallbackRun(e, angularModuleObject) {
        if (e.origin !== GAS_DOMAIN)
            return;
        var functionCallbackName = e.data.functionCallbackName;
        var functionCallbackParams = e.data.functionCallbackParams; //caso não funcione trocar por JSON.parse(functionRunParams)
        console.log("iframe(" + e.origin + ") solicitou ao iframe(" + window.location.href + "): googleCallbackrun(" + functionCallbackName + ")");
        angularModuleObject[functionCallbackName](functionCallbackParams);
    }
}
GAS.ɵfac = function GAS_Factory(t) { return new (t || GAS)(); };
GAS.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: GAS, factory: GAS.ɵfac, providedIn: 'root' });
// Request GAS
const REQUESTGAS = {
    functionName: '',
    functionParams: []
};
// Response do GAS
const RESPONSE = {
    response: null,
    message: null,
    messagerType: null
};
// Chamadas resource no GAS. Substituto da URL
const RESOURCE_EIXO = {
    functionName: 'MCPAMetamodelo.instance.SERVICE.GETEixos'
};
const RESOURCE_SEGMENTO = {
    functionName: 'MCPAMetamodelo.instance.SERVICE.GETTiposSegmentos'
};
const RESOURCE_NIVEISORGANIZACIONAIS = {
    functionName: 'MCPAMetamodelo.instance.SERVICE.GETTiposNiveisOrganizacionais'
};
const RESOURCE_PAINEL_APROPRIADOR = {
    functionName: 'MCPAInstrumentoPainelApropriador.instance.SERVICE.GETListarTodos'
};
const RESOURCE_TOPICOS_GETListarTodos = {
    functionName: 'MCPAInstrumentoDiagnosticadorTopico.instance.SERVICE.GETListarTodos'
};
const RESOURCE_ACAO_MELHORIA_GETListarTodos = {
    functionName: 'MCPAInstrumentoDiagnosticadorAcaoMelhoria.instance.SERVICE.GETListarTodos'
};


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _menu_menu_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./menu/menu.module */ "19mU");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");





class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_1__["AppRoutingModule"],
            _menu_menu_module__WEBPACK_IMPORTED_MODULE_3__["MenuModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_1__["AppRoutingModule"],
        _menu_menu_module__WEBPACK_IMPORTED_MODULE_3__["MenuModule"]] }); })();


/***/ }),

/***/ "byP8":
/*!*************************************************************************!*\
  !*** ./src/app/acoes-de-melhorias/acoes-de-melhorias-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: AcoesDeMelhoriasRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcoesDeMelhoriasRoutingModule", function() { return AcoesDeMelhoriasRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _acoes_de_melhorias_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./acoes-de-melhorias.component */ "p9TD");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");




const routes = [
    { path: 'acoesmelhorias',
        component: _acoes_de_melhorias_component__WEBPACK_IMPORTED_MODULE_1__["AcoesDeMelhoriasComponent"],
        data: {
            title: 'Açoes de Melhorias',
            breadcrumb: 'Ações de Melhorias'
        }
    },
];
class AcoesDeMelhoriasRoutingModule {
}
AcoesDeMelhoriasRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: AcoesDeMelhoriasRoutingModule });
AcoesDeMelhoriasRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ factory: function AcoesDeMelhoriasRoutingModule_Factory(t) { return new (t || AcoesDeMelhoriasRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](AcoesDeMelhoriasRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "fzUc":
/*!**************************************************************!*\
  !*** ./src/app/topicos/form-topico/form-topico.component.ts ***!
  \**************************************************************/
/*! exports provided: FormTopicoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormTopicoComponent", function() { return FormTopicoComponent; });
/* harmony import */ var src_app_core_loading__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/core/loading */ "xhfB");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_radiobutton__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/radiobutton */ "LuMj");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/dropdown */ "arFO");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_multiselect__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/multiselect */ "lVkt");
/* harmony import */ var primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/inputtextarea */ "zFJ7");











function FormTopicoComponent_ng_template_23_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const group_r2 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](group_r2.label);
} }
function FormTopicoComponent_p_radioButton_29_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-radioButton", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function FormTopicoComponent_p_radioButton_29_Template_p_radioButton_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r5); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r4.topico.nivelAplicavel = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const nivel_r3 = ctx.$implicit;
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("name", nivel_r3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("value", nivel_r3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("label", nivel_r3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.topico.nivelAplicavel);
} }
class FormTopicoComponent {
    constructor(ref, config) {
        this.ref = ref;
        this.config = config;
        this.dimensoes = [];
        this.selectedDimensoes = [];
        this.segmentos = [];
        this.selectedSegmentos = [];
        this.niveis = [];
    }
    ngOnInit() {
        this.getTopico();
        this.getDimensoes();
        this.getSegmentos();
        this.getNivelOrganizacional();
    }
    getTopico() {
        if (this.config.data != null) {
            this.topico = this.config.data;
        }
        ;
    }
    close() {
        this.ref.close();
    }
    confirme() {
        this.ref.close('confirm');
    }
    /**
     * Configura com a leitura vinda do GAS os eixos e dimensoes
     */
    getDimensoes() {
        let eixos = src_app_core_loading__WEBPACK_IMPORTED_MODULE_0__["Loading"].EIXOS.response;
        eixos.forEach(element => {
            let itens = [];
            element.dimensoes.forEach(element => {
                let dimensaolabel = element.numero + ' ' + element.nome;
                let dimensao = 'Dimensão ' + element.numero + ': ' + element.nome;
                itens.push({ label: dimensaolabel, value: dimensao });
            });
            let eixoDimensao = { label: element.nome, value: element.numero, items: itens };
            this.dimensoes.push(eixoDimensao);
        });
    }
    /**
     * Configura com a leitura vinda do GAS os segmentos
     */
    getSegmentos() {
        this.segmentos = [];
        src_app_core_loading__WEBPACK_IMPORTED_MODULE_0__["Loading"].SEGMENTOS.response.forEach(element => {
            this.segmentos.push({ segmento: element });
        });
        this.topico.segmentos.forEach(element => {
            this.selectedSegmentos.push({ segmento: element });
        });
    }
    /**
     * Configura com a leitura vinda do GAS os niveis organizacionais
     */
    getNivelOrganizacional() {
        this.niveis = [];
        src_app_core_loading__WEBPACK_IMPORTED_MODULE_0__["Loading"].NIVEIS_ORGANIZACIONAIS.response.forEach(element => {
            this.niveis.push(element);
        });
    }
    mostra(evento) {
        console.log(this.topico.segmentos);
    }
}
FormTopicoComponent.ɵfac = function FormTopicoComponent_Factory(t) { return new (t || FormTopicoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DynamicDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DynamicDialogConfig"])); };
FormTopicoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: FormTopicoComponent, selectors: [["app-form-topico"]], decls: 39, vars: 13, consts: [[1, "buttons"], ["pButton", "", "pRipple", "", "type", "button", "label", "SALVAR", 3, "click"], ["pButton", "", "pRipple", "", "type", "button", "label", "CANCELAR", 1, "espacoEsqueda", 3, "click"], [1, "espacoRow", "min"], [1, "block"], ["for", "radiosTipos", 1, "label"], ["id", "radiosTipos"], ["name", "tipo", "value", "REIVINDICA\u00C7\u00C3O", "inputId", "tipo1", "label", "REIVINDICA\u00C7\u00C3O", 3, "ngModel", "ngModelChange"], ["name", "tipo", "value", "AVALIA\u00C7\u00C3O INTERNA", "inputId", "tipo2", "label", "AVALIA\u00C7\u00C3O INTERNA", 1, "separadorEsqueda", 3, "ngModel", "ngModelChange"], ["name", "tipo", "value", "AVALIA\u00C7\u00C3O EXTERNA", "inputId", "tipo3", "label", "AVALIA\u00C7\u00C3O EXTERNA", 1, "separadorEsqueda", 3, "ngModel", "ngModelChange"], ["for", "radiosSentimentos", 1, "label"], ["id", "radiosSentimentos"], ["name", "sentimento", "value", "FRAGILIDADE", "inputId", "sentimento1", "label", "FRAGILIDADE", 3, "ngModel", "ngModelChange"], ["name", "sentimento", "value", "POTENCIALIDADE", "inputId", "sentimento2", "label", "POTENCIALIDADE", 1, "separadorEsqueda", 3, "ngModel", "ngModelChange"], ["for", "dropdownEixoDimensoes", 1, "label"], ["id", "dropdownEixoDimensoes", "appendTo", "body", "defaultLabel", "TODOS", "crollHeight", "250px", 3, "options", "group", "ngModel", "ngModelChange"], ["pTemplate", "group"], ["for", "radiosNivel", 1, "label"], ["id", "radiosNivel"], ["inputId", "nivel1", 3, "name", "value", "ngModel", "label", "ngModelChange", 4, "ngFor", "ngForOf"], ["for", "dropdownSegmentos", 1, "label"], ["id", "dropdownSegmentos", "appendTo", "body", "defaultLabel", "TODOS", "optionLabel", "segmento", "display", "chip", 3, "options", "ngModel", "ngModelChange", "onChange"], ["for", "textoToipico", 1, "label"], ["id", "textoToipico", "autoResize", "false", "maxlength", "500", "pInputTextarea", "", 3, "rows", "ngModel", "ngModelChange"], [1, "p-d-flex", "p-ai-center"], ["inputId", "nivel1", 3, "name", "value", "ngModel", "label", "ngModelChange"]], template: function FormTopicoComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FormTopicoComponent_Template_button_click_1_listener() { return ctx.confirme(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FormTopicoComponent_Template_button_click_2_listener() { return ctx.close(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "span", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "label", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "TIPO");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "p-radioButton", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function FormTopicoComponent_Template_p_radioButton_ngModelChange_8_listener($event) { return ctx.topico.tipoTopico = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "p-radioButton", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function FormTopicoComponent_Template_p_radioButton_ngModelChange_9_listener($event) { return ctx.topico.tipoTopico = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "p-radioButton", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function FormTopicoComponent_Template_p_radioButton_ngModelChange_10_listener($event) { return ctx.topico.tipoTopico = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "span", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "label", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14, "SENTIMENTO");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "p-radioButton", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function FormTopicoComponent_Template_p_radioButton_ngModelChange_16_listener($event) { return ctx.topico.sentimento = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "p-radioButton", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function FormTopicoComponent_Template_p_radioButton_ngModelChange_17_listener($event) { return ctx.topico.sentimento = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "span", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "label", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, "EIXO / DIMENS\u00C3O");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "p-dropdown", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function FormTopicoComponent_Template_p_dropdown_ngModelChange_22_listener($event) { return ctx.topico.dimensao = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](23, FormTopicoComponent_ng_template_23_Template, 3, 1, "ng-template", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "span", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "label", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](27, "NIVEL APLIC\u00C1VEL");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](29, FormTopicoComponent_p_radioButton_29_Template, 1, 4, "p-radioButton", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](31, "span", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "label", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](33, "SEGMENTOS");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "p-multiSelect", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function FormTopicoComponent_Template_p_multiSelect_ngModelChange_34_listener($event) { return ctx.selectedSegmentos = $event; })("onChange", function FormTopicoComponent_Template_p_multiSelect_onChange_34_listener($event) { return ctx.mostra($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](36, "label", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](37, "T\u00D3PICO / INDICADOR");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "textarea", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function FormTopicoComponent_Template_textarea_ngModelChange_38_listener($event) { return ctx.topico.topicoOuIndicador = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.topico.tipoTopico);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.topico.tipoTopico);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.topico.tipoTopico);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.topico.sentimento);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.topico.sentimento);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx.dimensoes)("group", true)("ngModel", ctx.topico.dimensao);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.niveis);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx.segmentos)("ngModel", ctx.selectedSegmentos);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("rows", 5)("ngModel", ctx.topico.topicoOuIndicador);
    } }, directives: [primeng_button__WEBPACK_IMPORTED_MODULE_3__["ButtonDirective"], primeng_radiobutton__WEBPACK_IMPORTED_MODULE_4__["RadioButton"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgModel"], primeng_dropdown__WEBPACK_IMPORTED_MODULE_6__["Dropdown"], primeng_api__WEBPACK_IMPORTED_MODULE_7__["PrimeTemplate"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgForOf"], primeng_multiselect__WEBPACK_IMPORTED_MODULE_9__["MultiSelect"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["MaxLengthValidator"], primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_10__["InputTextarea"]], styles: [".espacoEsqueda[_ngcontent-%COMP%] {\n  margin-left: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2Zvcm0tdG9waWNvLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZ0JBQUE7QUFDSiIsImZpbGUiOiJmb3JtLXRvcGljby5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5lc3BhY29Fc3F1ZWRhIHtcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xufSJdfQ== */"] });


/***/ }),

/***/ "gFHE":
/*!********************************************************!*\
  !*** ./src/app/comprovacoes/comprovacoes.component.ts ***!
  \********************************************************/
/*! exports provided: ComprovacoesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComprovacoesComponent", function() { return ComprovacoesComponent; });
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var _excluir_excluir_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../excluir/excluir.component */ "pJC4");
/* harmony import */ var _form_comprovacao_form_comprovacao_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./form-comprovacao/form-comprovacao.component */ "HUMg");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/table */ "rEr+");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/api */ "7zfz");









function ComprovacoesComponent_ng_template_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "th");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, "#");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "th", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](4, "COMPROVA\u00C7\u00D5ES (TEXTO OU LINK)");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](5, "th");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ComprovacoesComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "td", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "td", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "td", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "button", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ComprovacoesComponent_ng_template_7_Template_button_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r3.excluir(ctx_r3.item, "COMPROVA\u00C7\u00C3O"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const impedimento_r2 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](impedimento_r2.id);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](impedimento_r2.texto);
} }
class ComprovacoesComponent {
    constructor(router, dialogService) {
        this.router = router;
        this.dialogService = dialogService;
        this.lista = [];
    }
    ngOnInit() {
        this.getLista();
    }
    getLista() {
        let comprovacao = {
            id: 1,
            texto: 'https://ifpb.edu.br',
        };
        this.lista.push(comprovacao);
        let index = 2;
        comprovacao =
            {
                id: index,
                texto: 'https://url2',
            };
        index++;
        this.lista.push(comprovacao);
        comprovacao =
            {
                id: index,
                texto: 'https://url1',
            };
        this.lista.push(comprovacao);
    }
    show(titulo) {
        const ref = this.dialogService.open(_form_comprovacao_form_comprovacao_component__WEBPACK_IMPORTED_MODULE_2__["FormComprovacaoComponent"], {
            header: titulo,
            width: '100%',
            style: { "min-width": "720px", },
        });
        ref.onClose.subscribe(resposta => {
            if (resposta == 'confirm') {
                console.log('Confirmado.');
            }
        });
    }
    excluir(item, nomeitem) {
        const ref = this.dialogService.open(_excluir_excluir_component__WEBPACK_IMPORTED_MODULE_1__["ExcluirComponent"], {
            header: 'CONFIRMAR REMOÇÃO DA ' + nomeitem,
            data: {
                item: nomeitem,
            },
            width: '100%',
            // height: '100%',
            style: { "min-width": "720px", },
        });
        ref.onClose.subscribe(resposta => {
            if (resposta == 'confirm') {
                console.log('Confirmado para exclusão.');
            }
        });
    }
    click(url) {
        this.router.navigate([url]);
    }
}
ComprovacoesComponent.ɵfac = function ComprovacoesComponent_Factory(t) { return new (t || ComprovacoesComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__["DialogService"])); };
ComprovacoesComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: ComprovacoesComponent, selectors: [["app-comprovacoes"]], features: [_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵProvidersFeature"]([primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__["DialogService"]])], decls: 8, vars: 2, consts: [[1, "btnAdicionar"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-plus", "title", "Adiconar comprova\u00E7\u00E3o \u00E0 a\u00E7\u00E3o de melhoria", 1, "p-button-rounded", 3, "click"], [1, "botoesTopContent"], ["pButton", "", "pRipple", "", "type", "button", "label", "VOLTAR", 1, "espacoEsqueda", 3, "click"], [1, "card", 2, "margin-top", "50px"], ["scrollHeight", "500px", 3, "value", "scrollable"], ["pTemplate", "header"], ["pTemplate", "body"], [2, "width", "80%"], [2, "width", "25px"], ["pButton", "", "type", "button", "title", "Excluir comprova\u00E7\u00E3o", "icon", "bi bi-x", "iconPos", "left", "styleClass", "iconBotao", 1, "p-button-danger", 3, "click"]], template: function ComprovacoesComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ComprovacoesComponent_Template_button_click_1_listener() { return ctx.show("ADICIONAR COMPROVA\u00C7\u00C3O"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ComprovacoesComponent_Template_button_click_3_listener() { return ctx.click("/acoesmelhorias"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "p-table", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](6, ComprovacoesComponent_ng_template_6_Template, 6, 0, "ng-template", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](7, ComprovacoesComponent_ng_template_7_Template, 7, 2, "ng-template", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("value", ctx.lista)("scrollable", true);
    } }, directives: [primeng_button__WEBPACK_IMPORTED_MODULE_5__["ButtonDirective"], primeng_table__WEBPACK_IMPORTED_MODULE_6__["Table"], primeng_api__WEBPACK_IMPORTED_MODULE_7__["PrimeTemplate"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb21wcm92YWNvZXMuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "hNPd":
/*!*************************************************************************!*\
  !*** ./src/app/painel-apropriador/painel-apropriador-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: PainelApropriadorRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PainelApropriadorRoutingModule", function() { return PainelApropriadorRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _painel_apropriador_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./painel-apropriador.component */ "5pU/");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");




const routes = [
    { path: 'painelapropriador',
        component: _painel_apropriador_component__WEBPACK_IMPORTED_MODULE_1__["PainelApropriadorComponent"],
        data: {
            title: 'Painel Apropriador',
            breadcrumb: 'Painel Apropriador'
        }
    },
];
class PainelApropriadorRoutingModule {
}
PainelApropriadorRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: PainelApropriadorRoutingModule });
PainelApropriadorRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ factory: function PainelApropriadorRoutingModule_Factory(t) { return new (t || PainelApropriadorRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](PainelApropriadorRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "p9TD":
/*!********************************************************************!*\
  !*** ./src/app/acoes-de-melhorias/acoes-de-melhorias.component.ts ***!
  \********************************************************************/
/*! exports provided: AcoesDeMelhoriasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcoesDeMelhoriasComponent", function() { return AcoesDeMelhoriasComponent; });
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var _form_acao_melhoria_form_acao_melhoria_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./form-acao-melhoria/form-acao-melhoria.component */ "VLMe");
/* harmony import */ var _excluir_excluir_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../excluir/excluir.component */ "pJC4");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_dataview__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/dataview */ "8CEF");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var primeng_multiselect__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/multiselect */ "lVkt");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/card */ "QIUk");













function AcoesDeMelhoriasComponent_ng_template_4_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "label", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, "ESCALA AVALIATIVA");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "p-multiSelect", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngModelChange", function AcoesDeMelhoriasComponent_ng_template_4_Template_p_multiSelect_ngModelChange_3_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r3.selectedEscalas = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "span", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](5, "i", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "input", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("input", function AcoesDeMelhoriasComponent_ng_template_4_Template_input_input_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r4); _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵreference"](3); return _r0.filter($event.target.value); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("options", ctx_r1.escalas)("ngModel", ctx_r1.selectedEscalas);
} }
function AcoesDeMelhoriasComponent_ng_template_5_ng_template_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "table", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "td", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "span", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](4, " \u2630T\u00D3PICO : FALTA DE ESPA\u00C7OS DE CONVIV\u00CANCIA NO CAMPUS (BANCOS, SOMBRA, EQUIPAMENTOS DE JOGOS, SOM, CORREDOR... ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "td", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "span", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](7, " A\u00C7\u00C3O ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](8, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](9, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](10, "span", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](11, " \u2630INICIADA ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function AcoesDeMelhoriasComponent_ng_template_5_ng_template_14_Template(rf, ctx) { if (rf & 1) {
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "table", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "td", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "div", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](4, "giuseppe.lima@ifpb.edu.br (20/02/2021 12:45:38)");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "td", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "p-button", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function AcoesDeMelhoriasComponent_ng_template_5_ng_template_14_Template_p_button_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r10); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2); return ctx_r9.show("EDITAR A\u00C7\u00C3O"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "p-button", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function AcoesDeMelhoriasComponent_ng_template_5_ng_template_14_Template_p_button_click_7_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r10); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2); return ctx_r11.click("/impedimentos"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](8, "p-button", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function AcoesDeMelhoriasComponent_ng_template_5_ng_template_14_Template_p_button_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r10); const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2); return ctx_r12.click("/comprovacoes"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](9, "p-button", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function AcoesDeMelhoriasComponent_ng_template_5_ng_template_14_Template_p_button_click_9_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r10); const item_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]().$implicit; const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r13.excluir(item_r6, "A\u00C7\u00C3O"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function AcoesDeMelhoriasComponent_ng_template_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "p-card");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, AcoesDeMelhoriasComponent_ng_template_5_ng_template_1_Template, 12, 0, "ng-template", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "span", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "span", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, "QUAL?");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](7, " Indica\u00E7\u00E3o de espa\u00E7os de conviv\u00EAncia. ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](8, "> ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](9, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](10, "span", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](11, "COMO?");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](12, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](13, " Reuni\u00E3o com estudantes e coordenadores de cursos. ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](14, AcoesDeMelhoriasComponent_ng_template_5_ng_template_14_Template, 10, 0, "ng-template", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
class AcoesDeMelhoriasComponent {
    constructor(router, dialogService) {
        this.router = router;
        this.dialogService = dialogService;
        this.niveis = [];
        this.segmentos = [];
        this.dimensoes = [];
        this.sentimentos = [];
        this.tipos = [];
        this.acoes = [];
        this.escalas = [];
        this.cor = { texto: 'TXfragilidade', bg: 'BGfragilidade' };
        // cor = {texto: 'TXpotencialidade', bg:'BGpotencialidade'}
        this.lista = [];
    }
    ngOnInit() {
        this.getLista();
    }
    show(titulo) {
        const ref = this.dialogService.open(_form_acao_melhoria_form_acao_melhoria_component__WEBPACK_IMPORTED_MODULE_1__["FormAcaoMelhoriaComponent"], {
            header: titulo,
            width: '100%',
            style: { "min-width": "600px" },
        });
        ref.onClose.subscribe(resposta => {
            if (resposta == 'confirm') {
                console.log('Confirmado.');
            }
        });
    }
    click(url) {
        this.router.navigate([url]);
    }
    excluir(item, nomeitem) {
        const ref = this.dialogService.open(_excluir_excluir_component__WEBPACK_IMPORTED_MODULE_2__["ExcluirComponent"], {
            header: 'CONFIRMAR REMOÇÃO DA ' + nomeitem,
            data: {
                item: nomeitem,
            },
            width: '100%',
            // height: '100%',
            style: { "min-width": "720px", },
        });
        ref.onClose.subscribe(resposta => {
            if (resposta == 'confirm') {
                console.log('Confirmado para exclusão.');
            }
        });
    }
    getLista() {
        let acoes = this.fake();
        this.lista.push(acoes);
        this.getNiveis(this.lista);
        this.getSegmentos(this.lista);
        this.getDimensoes(this.lista);
        this.getSentimento(this.lista);
        this.getTipos(this.lista);
        this.getAcoes(this.lista);
        this.getEscalaAvaliativa(this.lista);
    }
    fake() {
        // let acoes = {
        //   dimensao: 'DIMENSÃO 6: ORGANIZAÇÃO E GESTÃO DA INSTITUIÇÃO',
        //   segmento: ['DOCENTE', 'DISCENTE'],
        //   nivel: 'CAMPUS-MT',
        //   curso: '502 - Tecnologia em Análise e Desenvolvimento de Sistemas - Monteiro (CAMPUS MONTEIRO)',
        //   potencialidade: {
        //     topicos: 999,
        //     dimensoes: 9,
        //   },
        //   fragilidades: {
        //     topicos: 999,
        //     dimensoes: 9,
        //   },
        // }
        return [
            {
                id: 1,
                emailUltimoRevisor: 'giuseppe.lima@ifpb.edu.br',
                dataUltimaAtualizacao: '20/02/2021 12:45:38',
                itemEscalaAvaliativa: { valorQuantitativo: 'ItemEscalaAvaliativa', valorQualitativo: '' },
                qual: 'Indicação de espaços de convivência.',
                como: 'Reunião com estudantes e coordenadores de cursos.',
                impedimentos: [
                    {
                        nrOrdem: 1,
                        impedimento: 'O orçamento LOA ainda não foi aprovado'
                    },
                    {
                        nrOrdem: 2,
                        impedimento: 'Não há pespectiva de disponibilidade financeira até o final do ano 2021'
                    },
                    {
                        nrOrdem: 3,
                        impedimento: 'https://ifpb.edu.br'
                    },
                ],
                comprovacoes: [
                    {
                        nrOrdem: 1,
                        impedimento: 'https://url2'
                    },
                    {
                        nrOrdem: 2,
                        impedimento: 'https://ifpb.edu.br'
                    },
                    {
                        nrOrdem: 3,
                        impedimento: 'https://url1'
                    },
                ]
            }
        ];
    }
    getNiveis(lista) {
        this.niveis.push({ nivel: 'TODOS' });
        this.niveis.push({ nivel: 'CAMPUS' });
        this.niveis.push({ nivel: 'CURSO' });
    }
    getSegmentos(lista) {
        this.segmentos.push({ segmento: 'DOCENTES' });
        this.segmentos.push({ segmento: 'GESTOR' });
        this.segmentos.push({ segmento: 'TEC. ADMINISTRATIVO' });
        lista.forEach(element => {
            this.segmentos.push({ segmento: element.segmento });
        });
    }
    getDimensoes(lista) {
        this.dimensoes = [
            {
                label: 'Eixo 1: Planejamento e Avaliação Institucional', value: 'eixo1',
                items: [
                    { label: 'Dimensão 8: Planejamento e Avaliação', value: 'dimensao8' },
                ]
            },
            {
                label: 'Eixo 2: Desenvolvimento Institucional', value: 'eixo2',
                items: [
                    { label: 'Dimensão 1: Missão e Plano de Desenvolvimento Institucional', value: 'dimensao1' },
                    { label: 'Dimensão 3: Responsabilidade Social da Instituição', value: 'dimensao3' },
                ]
            },
        ];
        // this.dimensoes.push({ segmento: 'TODOS' })
        // lista.forEach(element => {
        //   this.dimensoes.push({ dimensao: element.dimensao })
        // });
    }
    getSentimento(lista) {
        this.sentimentos.push({ sentimento: 'TODOS' });
        this.sentimentos.push({ sentimento: 'FRAGILIDADES' });
        this.sentimentos.push({ sentimento: 'POTENCIALIDADES' });
    }
    getTipos(lista) {
        this.tipos.push({ tipo: 'TODOS' });
        this.tipos.push({ tipo: 'REIVINDICAÇÃO' });
        this.tipos.push({ tipo: 'AVALIAÇÃO INTERNA' });
        this.tipos.push({ tipo: 'AVALIAÇÃO EXTERNA' });
    }
    getAcoes(lista) {
        this.acoes.push({ acao: 'TODOS' });
        this.acoes.push({ acao: 'SIM' });
        this.acoes.push({ acao: 'NÃO' });
    }
    getEscalaAvaliativa(lista) {
        this.escalas.push({ escala: 'NÃO AVALIADA' });
        this.escalas.push({ escala: 'DEBATIDA E PLANEJADA' });
        this.escalas.push({ escala: 'INICIADA' });
        this.escalas.push({ escala: 'IMPLANTADA PARCIALMENTE' });
        this.escalas.push({ escala: 'IMPLANTADA TOTALMENTE' });
    }
}
AcoesDeMelhoriasComponent.ɵfac = function AcoesDeMelhoriasComponent_Factory(t) { return new (t || AcoesDeMelhoriasComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__["DialogService"])); };
AcoesDeMelhoriasComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: AcoesDeMelhoriasComponent, selectors: [["app-acoes-de-melhorias"]], features: [_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵProvidersFeature"]([primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_0__["DialogService"]])], decls: 6, vars: 3, consts: [[1, "btnAdicionar"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-plus", "title", "Adiconar a\u00E7\u00E3o de melhoria", 1, "p-button-rounded", 3, "click"], ["paginatorPosition", "both", "filterBy", "ciclo", "layout", "grid", 3, "value", "paginator", "rows"], ["dv", ""], ["pTemplate", "header"], ["pTemplate", "gridItem"], [1, "block"], ["for", "dropdownEscala", 1, "label"], ["id", "dropdownEscala", "styleClass", "eixoCSS", "defaultLabel", "TODOS", "optionLabel", "escala", "display", "chip", 3, "options", "ngModel", "ngModelChange"], [1, "p-input-icon-left"], [1, "pi", "pi-search"], ["type", "search", "pInputText", "", "placeholder", "Pesquisar", 3, "input"], [1, "bodyTopico"], [1, "corQual"], [1, "questao"], ["pTemplate", "footer"], [1, "tabela"], [1, "col80"], [1, "tagTitulo"], ["rowspan", "3"], [1, "tituloCard"], [1, "tagSubtitulo"], [1, "revisado"], [1, "botoesCard"], ["icon", "bi bi-gear-fill", "iconPos", "left", "title", "Editar t\u00F3picos", "styleClass", "iconBotao", 3, "click"], ["icon", "bi bi-exclamation", "iconPos", "left", "title", "Impedimentos", "styleClass", "iconBotao", 1, "espacoEsqueda", 3, "click"], ["icon", "bi bi-paperclip", "iconPos", "left", "title", "Comprova\u00E7\u00F5es", "styleClass", "iconBotao", 1, "espacoEsqueda", 3, "click"], ["icon", "bi bi-trash-fill", "iconPos", "left", "title", "Excluir t\u00F3picos", "styleClass", "iconBotao", 1, "espacoEsqueda", 3, "click"]], template: function AcoesDeMelhoriasComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function AcoesDeMelhoriasComponent_Template_button_click_1_listener() { return ctx.show("ADICIONAR A\u00C7\u00C3O"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "p-dataView", 2, 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](4, AcoesDeMelhoriasComponent_ng_template_4_Template, 7, 2, "ng-template", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](5, AcoesDeMelhoriasComponent_ng_template_5_Template, 15, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("value", ctx.lista)("paginator", true)("rows", 9);
    } }, directives: [primeng_button__WEBPACK_IMPORTED_MODULE_5__["ButtonDirective"], primeng_dataview__WEBPACK_IMPORTED_MODULE_6__["DataView"], primeng_api__WEBPACK_IMPORTED_MODULE_7__["PrimeTemplate"], primeng_multiselect__WEBPACK_IMPORTED_MODULE_8__["MultiSelect"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgModel"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_10__["InputText"], primeng_card__WEBPACK_IMPORTED_MODULE_11__["Card"], primeng_button__WEBPACK_IMPORTED_MODULE_5__["Button"]], styles: ["[_nghost-%COMP%]     .iconBotao {\n  font-size: 1.5rem !important;\n  padding-bottom: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL2Fjb2VzLWRlLW1lbGhvcmlhcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLDRCQUFBO0VBQ0EsbUJBQUE7QUFBUiIsImZpbGUiOiJhY29lcy1kZS1tZWxob3JpYXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgOjpuZy1kZWVwIC5pY29uQm90YW8ge1xuICAgICAgICBmb250LXNpemU6IDEuNXJlbSAhaW1wb3J0YW50O1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICAgIH0gICAgXG59Il19 */"] });


/***/ }),

/***/ "pJC4":
/*!**********************************************!*\
  !*** ./src/app/excluir/excluir.component.ts ***!
  \**********************************************/
/*! exports provided: ExcluirComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExcluirComponent", function() { return ExcluirComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/button */ "jIHw");



class ExcluirComponent {
    constructor(ref, config) {
        this.ref = ref;
        this.config = config;
    }
    ngOnInit() {
        this.item = this.config.data.item;
    }
    close() {
        this.ref.close();
    }
    confirme() {
        this.ref.close('confirm');
    }
}
ExcluirComponent.ɵfac = function ExcluirComponent_Factory(t) { return new (t || ExcluirComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_1__["DynamicDialogRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_1__["DynamicDialogConfig"])); };
ExcluirComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ExcluirComponent, selectors: [["app-excluir"]], decls: 10, vars: 1, consts: [[1, "buttons"], ["pButton", "", "pRipple", "", "type", "button", "label", "CONFIRMA", 3, "click"], ["pButton", "", "pRipple", "", "type", "button", "label", "CANCELAR", 1, "espacoEsqueda", 3, "click"], [1, "block"], ["src", "../../assets/icons/alerta.svg", 1, "alerta"]], template: function ExcluirComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExcluirComponent_Template_button_click_1_listener() { return ctx.confirme(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ExcluirComponent_Template_button_click_2_listener() { return ctx.close(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " DESEJA REALMENTE REMOV\u00CA-LO? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" AO CONFIRMAR A REMO\u00C7\u00C3O DESTE ", ctx.item, " ELE N\u00C3O PODER\u00C1 MAIS SER RECUPERADO. ");
    } }, directives: [primeng_button__WEBPACK_IMPORTED_MODULE_2__["ButtonDirective"]], styles: [".alerta[_ngcontent-%COMP%] {\n  padding-top: 30px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL2V4Y2x1aXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtBQUNKIiwiZmlsZSI6ImV4Y2x1aXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYWxlcnRhIHtcbiAgICBwYWRkaW5nLXRvcDogMzBweDtcbn0iXX0= */"] });


/***/ }),

/***/ "tRTB":
/*!*****************************************************!*\
  !*** ./src/app/impedimentos/impedimentos.module.ts ***!
  \*****************************************************/
/*! exports provided: ImpedimentosModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImpedimentosModule", function() { return ImpedimentosModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _impedimentos_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./impedimentos-routing.module */ "GHM7");
/* harmony import */ var _impedimentos_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./impedimentos.component */ "NXVx");
/* harmony import */ var _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/primeng.module */ "Cp9f");
/* harmony import */ var _form_impedimento_form_impedimento_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./form-impedimento/form-impedimento.component */ "Cxho");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "fXoL");






class ImpedimentosModule {
}
ImpedimentosModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({ type: ImpedimentosModule });
ImpedimentosModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({ factory: function ImpedimentosModule_Factory(t) { return new (t || ImpedimentosModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__["PrimengModule"],
            _impedimentos_routing_module__WEBPACK_IMPORTED_MODULE_1__["ImpedimentosRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsetNgModuleScope"](ImpedimentosModule, { declarations: [_impedimentos_component__WEBPACK_IMPORTED_MODULE_2__["ImpedimentosComponent"], _form_impedimento_form_impedimento_component__WEBPACK_IMPORTED_MODULE_4__["FormImpedimentoComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__["PrimengModule"],
        _impedimentos_routing_module__WEBPACK_IMPORTED_MODULE_1__["ImpedimentosRoutingModule"]], exports: [_impedimentos_component__WEBPACK_IMPORTED_MODULE_2__["ImpedimentosComponent"]] }); })();


/***/ }),

/***/ "ud/B":
/*!*****************************************************************!*\
  !*** ./src/app/painel-apropriador/painel-apropriador.module.ts ***!
  \*****************************************************************/
/*! exports provided: PainelApropriadorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PainelApropriadorModule", function() { return PainelApropriadorModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _painel_apropriador_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./painel-apropriador-routing.module */ "hNPd");
/* harmony import */ var _painel_apropriador_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./painel-apropriador.component */ "5pU/");
/* harmony import */ var _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/primeng.module */ "Cp9f");
/* harmony import */ var _topicos_topicos_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../topicos/topicos.module */ "Cqa6");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "fXoL");






class PainelApropriadorModule {
}
PainelApropriadorModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({ type: PainelApropriadorModule });
PainelApropriadorModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({ factory: function PainelApropriadorModule_Factory(t) { return new (t || PainelApropriadorModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__["PrimengModule"],
            _painel_apropriador_routing_module__WEBPACK_IMPORTED_MODULE_1__["PainelApropriadorRoutingModule"],
            _topicos_topicos_module__WEBPACK_IMPORTED_MODULE_4__["TopicosModule"],
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsetNgModuleScope"](PainelApropriadorModule, { declarations: [_painel_apropriador_component__WEBPACK_IMPORTED_MODULE_2__["PainelApropriadorComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__["PrimengModule"],
        _painel_apropriador_routing_module__WEBPACK_IMPORTED_MODULE_1__["PainelApropriadorRoutingModule"],
        _topicos_topicos_module__WEBPACK_IMPORTED_MODULE_4__["TopicosModule"]], exports: [_painel_apropriador_component__WEBPACK_IMPORTED_MODULE_2__["PainelApropriadorComponent"]] }); })();


/***/ }),

/***/ "vKON":
/*!*****************************************************!*\
  !*** ./src/app/comprovacoes/comprovacoes.module.ts ***!
  \*****************************************************/
/*! exports provided: ComprovacoesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComprovacoesModule", function() { return ComprovacoesModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _comprovacoes_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./comprovacoes-routing.module */ "8FQr");
/* harmony import */ var _comprovacoes_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./comprovacoes.component */ "gFHE");
/* harmony import */ var _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/primeng.module */ "Cp9f");
/* harmony import */ var _form_comprovacao_form_comprovacao_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./form-comprovacao/form-comprovacao.component */ "HUMg");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "fXoL");






class ComprovacoesModule {
}
ComprovacoesModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({ type: ComprovacoesModule });
ComprovacoesModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({ factory: function ComprovacoesModule_Factory(t) { return new (t || ComprovacoesModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__["PrimengModule"],
            _comprovacoes_routing_module__WEBPACK_IMPORTED_MODULE_1__["ComprovacoesRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsetNgModuleScope"](ComprovacoesModule, { declarations: [_comprovacoes_component__WEBPACK_IMPORTED_MODULE_2__["ComprovacoesComponent"], _form_comprovacao_form_comprovacao_component__WEBPACK_IMPORTED_MODULE_4__["FormComprovacaoComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _core_primeng_module__WEBPACK_IMPORTED_MODULE_3__["PrimengModule"],
        _comprovacoes_routing_module__WEBPACK_IMPORTED_MODULE_1__["ComprovacoesRoutingModule"]], exports: [_comprovacoes_component__WEBPACK_IMPORTED_MODULE_2__["ComprovacoesComponent"]] }); })();


/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");



const routes = [];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes, { useHash: true })], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "xhfB":
/*!*********************************!*\
  !*** ./src/app/core/loading.ts ***!
  \*********************************/
/*! exports provided: Loading */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Loading", function() { return Loading; });
/* harmony import */ var _GAS__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./GAS */ "Y7Y+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");




class Loading {
    constructor(gas) {
        this.gas = gas;
        this.start();
    }
    start() {
        console.log('Ler parâmetros de confifuração... (core loading.ts)');
        this.eixos();
        this.semGas();
    }
    eixos() {
        _GAS__WEBPACK_IMPORTED_MODULE_0__["REQUESTGAS"].functionName = _GAS__WEBPACK_IMPORTED_MODULE_0__["RESOURCE_EIXO"].functionName;
        _GAS__WEBPACK_IMPORTED_MODULE_0__["REQUESTGAS"].functionParams = [];
        this.gas.GASRequest(_GAS__WEBPACK_IMPORTED_MODULE_0__["REQUESTGAS"], this.resultadoEixos, this);
    }
    resultadoEixos(response) {
        Loading.EIXOS = response;
        console.log('Eixos');
        console.log(response);
        // Buscar lista de segmento
        this.segmentos();
    }
    segmentos() {
        _GAS__WEBPACK_IMPORTED_MODULE_0__["REQUESTGAS"].functionName = _GAS__WEBPACK_IMPORTED_MODULE_0__["RESOURCE_SEGMENTO"].functionName;
        _GAS__WEBPACK_IMPORTED_MODULE_0__["REQUESTGAS"].functionParams = [];
        this.gas.GASRequest(_GAS__WEBPACK_IMPORTED_MODULE_0__["REQUESTGAS"], this.resultadoSegmentos, this);
    }
    resultadoSegmentos(response) {
        Loading.SEGMENTOS = response;
        console.log('Segmentos');
        console.log(response);
        // Buscar lista de tipo de niveis organizacionais
        this.niveisOrganizacionais();
    }
    niveisOrganizacionais() {
        _GAS__WEBPACK_IMPORTED_MODULE_0__["REQUESTGAS"].functionName = _GAS__WEBPACK_IMPORTED_MODULE_0__["RESOURCE_NIVEISORGANIZACIONAIS"].functionName;
        _GAS__WEBPACK_IMPORTED_MODULE_0__["REQUESTGAS"].functionParams = [];
        this.gas.GASRequest(_GAS__WEBPACK_IMPORTED_MODULE_0__["REQUESTGAS"], this.resultadoNiveisOrganizacionais, this);
    }
    resultadoNiveisOrganizacionais(response) {
        Loading.NIVEIS_ORGANIZACIONAIS = response;
        console.log('Niveis Organizacionais');
        console.log(response);
    }
    semGas() {
        Loading.EIXOS = {
            "messageType": "INFO",
            "message": "OK",
            "response": [
                {
                    "dimensoes": [
                        {
                            "nome": "Planejamento e Avaliação",
                            "eixo": {},
                            "numero": 8
                        }
                    ],
                    "nome": "Planejamento e Avaliação Institucional",
                    "numero": 1
                },
                {
                    "dimensoes": [
                        {
                            "nome": "Missão e Plano de Desenvolvimento Institucional",
                            "numero": 1,
                            "eixo": {}
                        },
                        {
                            "nome": "Responsabilidade Social da Instituição",
                            "eixo": {},
                            "numero": 3
                        }
                    ],
                    "nome": "Desenvolvimento Institucional",
                    "numero": 2
                },
                {
                    "dimensoes": [
                        {
                            "nome": "Políticas para o Ensino, a Pesquisa e a Extensão",
                            "numero": 2,
                            "eixo": {}
                        },
                        {
                            "nome": "Comunicação com a Sociedade",
                            "numero": 4,
                            "eixo": {}
                        },
                        {
                            "nome": "Política de Atendimento aos Discentes",
                            "eixo": {},
                            "numero": 9
                        }
                    ],
                    "nome": "Políticas Acadêmicas",
                    "numero": 3
                },
                {
                    "dimensoes": [
                        {
                            "nome": "Políticas de Pessoal",
                            "eixo": {},
                            "numero": 5
                        },
                        {
                            "nome": "Organização e Gestão da Instituição",
                            "eixo": {},
                            "numero": 6
                        },
                        {
                            "nome": "Sustentabilidade Financeira",
                            "eixo": {},
                            "numero": 10
                        }
                    ],
                    "nome": "Políticas de Gestão",
                    "numero": 4
                },
                {
                    "dimensoes": [
                        {
                            "nome": "Infraestrutura Física",
                            "eixo": {},
                            "numero": 7
                        }
                    ],
                    "nome": "Infraestrutura Física",
                    "numero": 5
                }
            ]
        };
        Loading.SEGMENTOS = {
            "messageType": "INFO",
            "message": "OK",
            "response": [
                "DISCENTE",
                "EGRESSO",
                "DOCENTE",
                "TÉCNICO ADMINISTRATIVO",
                "SOCIEDADE",
                "GESTOR"
            ]
        };
        Loading.NIVEIS_ORGANIZACIONAIS = {
            "messageType": "INFO",
            "message": "OK",
            "response": [
                "INSTITUCIONAL",
                "CAMPUS",
                "CURSO",
                "DISCIPLINA",
                "SETOR"
            ]
        };
    }
}
Loading.ɵfac = function Loading_Factory(t) { return new (t || Loading)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_GAS__WEBPACK_IMPORTED_MODULE_0__["GAS"])); };
Loading.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: Loading, factory: Loading.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map