Projeto AVIN DEMO  
Desenvolvido pela equipe de desenvolvimento experimento para produto de software do TCC  

Front-End  
Luciano Azevedo  

Ano 2021  
IFPB - Campus Monteiro/PB  


# Clonar o projeto  
git clone https://gitlab.com/lucianocomputador/avin-demo  

# Acionar um servidor http  
python3 -m http.server 4200  
